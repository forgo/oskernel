
package com.gcloud.header.enums;

public enum ProviderType {

    GCLOUD(0),
    CINDER(1),
    GLANCE(2),
    NEUTRON(3);

    private int value;

    ProviderType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
