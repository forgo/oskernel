package com.gcloud.header.image.model;

import com.gcloud.framework.db.jdbc.annotation.TableField;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by yaowj on 2018/11/22.
 */
public class ImageType implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("id")
    private String imageId;
    @TableField("name")
    private String imageName;
    private String description;
    private Long size;
    private String status;
    private Date creationTime;
    private String architecture;
    private String imageOwnerAlias;

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getArchitecture() {
        return architecture;
    }

    public void setArchitecture(String architecture) {
        this.architecture = architecture;
    }

    public String getImageOwnerAlias() {
        return imageOwnerAlias;
    }

    public void setImageOwnerAlias(String imageOwnerAlias) {
        this.imageOwnerAlias = imageOwnerAlias;
    }
}
