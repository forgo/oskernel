package com.gcloud.header.network.model;

import java.io.Serializable;
import java.util.List;

public class DescribeVSwitchesResponse implements Serializable{
	private List<VSwitchSetType> vSwitch;

	public List<VSwitchSetType> getvSwitch() {
		return vSwitch;
	}

	public void setvSwitch(List<VSwitchSetType> vSwitch) {
		this.vSwitch = vSwitch;
	}
	
}
