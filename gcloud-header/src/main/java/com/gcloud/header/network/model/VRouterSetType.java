package com.gcloud.header.network.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;

public class VRouterSetType implements Serializable{
	@ApiModel(description = "路由器Id")
	private String vRouterId;
	@ApiModel(description = "路由器名称")
	private String vRouterName;
	@ApiModel(description = "区域Id")
	private String regionId;
	private String status;
//	private String cnStatus;
	private String subnets;
	
	public String getvRouterId() {
		return vRouterId;
	}
	public void setvRouterId(String vRouterId) {
		this.vRouterId = vRouterId;
	}
	public String getvRouterName() {
		return vRouterName;
	}
	public void setvRouterName(String vRouterName) {
		this.vRouterName = vRouterName;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
//	public String getCnStatus() {
//		return cnStatus;
//	}
//	public void setCnStatus(String cnStatus) {
//		this.cnStatus = cnStatus;
//	}
	public String getSubnets() {
		return subnets;
	}
	public void setSubnets(String subnets) {
		this.subnets = subnets;
	}
	
}
