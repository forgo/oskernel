package com.gcloud.header.network.msg.api;

import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class CreateVSwitchMsg extends ApiMessage {
	@NotBlank(message = "0030101")
	@ApiModel(description = "网段，形如1.1.1.1/20", require = true)
	private String cidrBlock;
	@ApiModel(description = "指定VSwitch所在的 VPC")
	private String vpcId;
	@ApiModel(description = "非VPC模式网络ID")
	private String NetworkId;
	@NotBlank(message = "0030103")
	@ApiModel(description = "交换机名称", require = true)
	private String vSwitchName;
	@ApiModel(description = "VSwitch所属区的ID")
	private String zoneId;
	@ApiModel(description = "网关IP")
	private String gateWayIp;
	
	@Override
	public Class replyClazz() {
		return CreateVSwitchReplyMsg.class;
	}
	
	public String getCidrBlock() {
		return cidrBlock;
	}
	public void setCidrBlock(String cidrBlock) {
		this.cidrBlock = cidrBlock;
	}
	public String getVpcId() {
		return vpcId;
	}
	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}
	public String getvSwitchName() {
		return vSwitchName;
	}
	public void setvSwitchName(String vSwitchName) {
		this.vSwitchName = vSwitchName;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public String getNetworkId() {
		return NetworkId;
	}

	public void setNetworkId(String networkId) {
		NetworkId = networkId;
	}

	public String getGateWayIp() {
		return gateWayIp;
	}

	public void setGateWayIp(String gateWayIp) {
		this.gateWayIp = gateWayIp;
	}
	
}
