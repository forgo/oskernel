package com.gcloud.header.network.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gcloud.framework.db.jdbc.annotation.TableField;

import java.io.Serializable;

public class NetworkInterfaceSet  implements Serializable{
	@TableField("id")
	private String networkInterfaceId; // 弹性网卡的 ID
	@TableField("name")
	private String networkInterfaceName; // 弹性网卡的名称
	@TableField("create_time")
	private String creationTime;
	private SecurityGroupIdSetType securityGroupIds;
	@TableField("subnet_id")
	private String vSwitchId;
	@TableField("network_id")
	private String vpcId;
	private String macAddress; // 弹性网卡的 MAC 地址
	@TableField("ip_address")
	private String privateIpAddress; // 弹性网卡主私有 IP 地址
	private String status; // 弹性网卡的状态
	@TableField("device_id")
	private String instanceId; // 弹性网卡当前关联的实例 ID
	@TableField("device_owner")
	private String instanceType; // 弹性网卡当前关联的实例 ID

	@JsonIgnore
	private String securityGroupIdsStr;

	public String getNetworkInterfaceId() {
		return networkInterfaceId;
	}

	public void setNetworkInterfaceId(String networkInterfaceId) {
		this.networkInterfaceId = networkInterfaceId;
	}

	public String getPrivateIpAddress() {
		return privateIpAddress;
	}

	public void setPrivateIpAddress(String privateIpAddress) {
		this.privateIpAddress = privateIpAddress;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}


	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getInstanceType() {
		return instanceType;
	}

	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}

	public String getNetworkInterfaceName() {
		return networkInterfaceName;
	}

	public void setNetworkInterfaceName(String networkInterfaceName) {
		this.networkInterfaceName = networkInterfaceName;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}


	public String getvSwitchId() {
		return vSwitchId;
	}

	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}

	public String getVpcId() {
		return vpcId;
	}

	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}

	public SecurityGroupIdSetType getSecurityGroupIds() {
		return securityGroupIds;
	}

	public void setSecurityGroupIds(SecurityGroupIdSetType securityGroupIds) {
		this.securityGroupIds = securityGroupIds;
	}

	public String getSecurityGroupIdsStr() {
		return securityGroupIdsStr;
	}

	public void setSecurityGroupIdsStr(String securityGroupIdsStr) {
		this.securityGroupIdsStr = securityGroupIdsStr;
	}
}
