package com.gcloud.header.network.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;

public class VSwitchSetType implements Serializable{
	@ApiModel(description = "交换机ID")
	private String vSwitchId;
	@ApiModel(description = "交换机所在的专有网络ID")
	private String vpcId;
	@ApiModel(description = "交换机的地址")
	private String cidrBlock;
	@ApiModel(description = "交换机名字")
	private String vSwitchName;
	@ApiModel(description = "可用区ID")
	private String zoneId;
	@ApiModel(description = "关联的路由ID")
	private String vRouterId;
	
	public String getvSwitchId() {
		return vSwitchId;
	}
	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}
	public String getVpcId() {
		return vpcId;
	}
	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}
	public String getCidrBlock() {
		return cidrBlock;
	}
	public void setCidrBlock(String cidrBlock) {
		this.cidrBlock = cidrBlock;
	}
	public String getvSwitchName() {
		return vSwitchName;
	}
	public void setvSwitchName(String vSwitchName) {
		this.vSwitchName = vSwitchName;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getvRouterId() {
		return vRouterId;
	}
	public void setvRouterId(String vRouterId) {
		this.vRouterId = vRouterId;
	}
	
}
