package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiReplyMessage;

public class CreateExternalNetworkReplyMsg extends ApiReplyMessage{
	
	private static final long serialVersionUID = 1L;
	private String networkId;
	
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

}
