package com.gcloud.header.network.model;

import java.io.Serializable;
import java.util.List;

public class DescribeEipAddressesResponse implements Serializable{
	private List<EipAddressSetType> eipAddresses;

	public List<EipAddressSetType> getEipAddresses() {
		return eipAddresses;
	}

	public void setEipAddresses(List<EipAddressSetType> eipAddresses) {
		this.eipAddresses = eipAddresses;
	}
	
}
