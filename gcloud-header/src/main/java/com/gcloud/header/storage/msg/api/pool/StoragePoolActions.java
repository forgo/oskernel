
package com.gcloud.header.storage.msg.api.pool;

public class StoragePoolActions {

    public static final String DESCRIBE_DISK_CATEGORIES = "DescribeDiskCategories";
    public static final String CREATE_DISK_CATEGORRY = "CreateDiskCategory";

    public static final String DESCRIBE_STORAGE_POOLS = "DescribeStoragePools";
    public static final String CREATE_STORAGE_POOL = "CreateStoragePool";
    public static final String MODIFY_STORAGE_POOL = "ModifyStoragePool";
    public static final String DELETE_STORAGE_POOL = "DeleteStoragePool";

}
