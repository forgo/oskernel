
package com.gcloud.header.storage;

public class StorageErrorCodes {

    // COMMON
    public static final String NO_SUCH_PROVIDER = "xxxx";
    public static final String NO_SUCH_STORAGE_TYPE = "xxxx";
    public static final String OPERATION_NO_SUPPORTED = "xxxx";

    // POOL ERRORCODE
    public static final String FAILED_TO_FIND_POOL = "xxxx";
    public static final String POOL_ALREADY_EXISTS = "xxxx";
    public static final String FAILED_TO_CREATE_POOL = "xxxx";
    public static final String FAILED_TO_DELETE_POOL = "xxxx";

    // VOLUME ERRORCODE
    public static final String FAILED_TO_CREATE_VOLUME = "xxxx";
    public static final String FAILED_TO_DELETE_VOLUME = "xxxx";
    public static final String FAILED_TO_RESIZE_VOLUME = "xxxx";
    public static final String FAILED_TO_ATTACH_VOLUME = "xxxx";
    public static final String FAILED_TO_DETACH_VOLUME = "xxxx";
    public static final String FAILED_TO_FIND_VOLUME = "0060503::找不到对应的磁盘";
    public static final String FAILED_TO_CREATE_SNAP = "xxxx";
    public static final String NEW_SIZE_CANNOT_BE_SMALLER = "0060504::磁盘大小要大于原来大小";
    public static final String VOLUME_IS_ATTACHED = "0060505::磁盘正在被使用，不能扩容";
    public static final String VOLUME_IS_NOT_AVAILABLE = "xxxx";
    public static final String VOLUME_IS_NOT_IN_USE = "xxxx";
    public static final String VOLUME_IS_NOT_ATTACHING = "xxxx";
    public static final String VOLUME_IS_NOT_DETACHING = "xxxx";

    // SNAPSHOT ERRORCODE
    public static final String SNAPSHOT_NOT_FOUND = "0060104::找不到对应的快照";

}
