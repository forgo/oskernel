package com.gcloud.header.storage.msg.api.volume;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by yaowj on 2018/9/29.
 */
public class ApiModifyDiskAttributeMsg extends ApiMessage {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "0060201::磁盘ID不能为空")
    private String diskId;
    @Length(max = 255, message = "0060203::名称长度不能大于255")
    private String diskName;

    @Override
    public Class replyClazz() {
        return ApiReplyMessage.class;
    }

    public String getDiskId() {
        return diskId;
    }

    public void setDiskId(String diskId) {
        this.diskId = diskId;
    }

    public String getDiskName() {
        return diskName;
    }

    public void setDiskName(String diskName) {
        this.diskName = diskName;
    }
}
