package com.gcloud.header.slb.msg.api;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.slb.model.BackendServerResponse;

public class ApiDescribeVServerGroupAttributeReplyMsg extends ApiReplyMessage{
	private String VServerGroupId;
	private String VServerGroupName;
	private BackendServerResponse backendServers;
	public String getVServerGroupId() {
		return VServerGroupId;
	}
	public void setVServerGroupId(String vServerGroupId) {
		VServerGroupId = vServerGroupId;
	}
	public String getVServerGroupName() {
		return VServerGroupName;
	}
	public void setVServerGroupName(String vServerGroupName) {
		VServerGroupName = vServerGroupName;
	}
	public BackendServerResponse getBackendServers() {
		return backendServers;
	}
	public void setBackendServers(BackendServerResponse backendServers) {
		this.backendServers = backendServers;
	}
}
