package com.gcloud.header.slb.msg.api;

import com.gcloud.header.ApiReplyMessage;

public class CreateLoadBalancerReplyMsg extends ApiReplyMessage {
	
	private String loadBalancerId;
	private String address;
	public String getLoadBalancerId() {
		return loadBalancerId;
	}
	public void setLoadBalancerId(String loadBalancerId) {
		this.loadBalancerId = loadBalancerId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	

}
