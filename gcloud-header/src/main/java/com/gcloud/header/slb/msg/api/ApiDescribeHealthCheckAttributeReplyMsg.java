package com.gcloud.header.slb.msg.api;

import com.gcloud.header.ApiReplyMessage;

public class ApiDescribeHealthCheckAttributeReplyMsg extends ApiReplyMessage {
	private Integer unhealthyThreshold;
	private Integer healthCheckTimeout;
	private Integer healthCheckInterval;
	private String healthCheck;
	private String healthCheckURI;
	private Integer healthyThreshold;
	private String healthCheckType;
	public Integer getUnhealthyThreshold() {
		return unhealthyThreshold;
	}
	public void setUnhealthyThreshold(Integer unhealthyThreshold) {
		this.unhealthyThreshold = unhealthyThreshold;
	}
	public Integer getHealthCheckTimeout() {
		return healthCheckTimeout;
	}
	public void setHealthCheckTimeout(Integer healthCheckTimeout) {
		this.healthCheckTimeout = healthCheckTimeout;
	}
	public Integer getHealthCheckInterval() {
		return healthCheckInterval;
	}
	public void setHealthCheckInterval(Integer healthCheckInterval) {
		this.healthCheckInterval = healthCheckInterval;
	}
	public String getHealthCheck() {
		return healthCheck;
	}
	public void setHealthCheck(String healthCheck) {
		this.healthCheck = healthCheck;
	}
	public String getHealthCheckURI() {
		return healthCheckURI;
	}
	public void setHealthCheckURI(String healthCheckURI) {
		this.healthCheckURI = healthCheckURI;
	}
	public Integer getHealthyThreshold() {
		return healthyThreshold;
	}
	public void setHealthyThreshold(Integer healthyThreshold) {
		this.healthyThreshold = healthyThreshold;
	}
	public String getHealthCheckType() {
		return healthCheckType;
	}
	public void setHealthCheckType(String healthCheckType) {
		this.healthCheckType = healthCheckType;
	}
}
