package com.gcloud.header.slb.msg.api;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.ApiReplyMessage;

public class ApiDescribeLoadBalancerAttributeReplyMsg extends ApiReplyMessage {
	
	private static final long serialVersionUID = 1L;
	
	private String loadBalancerId;
	private String loadBalancerName;
	private String loadBalancerStatus;
	private String address;
	private String vpcId;
	private String vSwitchId;
	private String createTime;
	private List<Integer> ListenerPorts;
	private List<ListenerPortAndProtocol> ListenerPortsAndProtocol;
	
	public String getLoadBalancerId() {
		return loadBalancerId;
	}
	public void setLoadBalancerId(String loadBalancerId) {
		this.loadBalancerId = loadBalancerId;
	}
	public String getLoadBalancerName() {
		return loadBalancerName;
	}
	public void setLoadBalancerName(String loadBalancerName) {
		this.loadBalancerName = loadBalancerName;
	}
	public String getLoadBalancerStatus() {
		return loadBalancerStatus;
	}
	public void setLoadBalancerStatus(String loadBalancerStatus) {
		this.loadBalancerStatus = loadBalancerStatus;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getVpcId() {
		return vpcId;
	}
	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}
	public String getvSwitchId() {
		return vSwitchId;
	}
	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public List<Integer> getListenerPorts() {
		return ListenerPorts;
	}
	public void setListenerPorts(List<Integer> listenerPorts) {
		ListenerPorts = listenerPorts;
	}
	public List<ListenerPortAndProtocol> getListenerPortsAndProtocol() {
		return ListenerPortsAndProtocol;
	}
	public void setListenerPortsAndProtocol(List<ListenerPortAndProtocol> listenerPortsAndProtocol) {
		ListenerPortsAndProtocol = listenerPortsAndProtocol;
	}
	
	

}
