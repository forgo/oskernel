package com.gcloud.header.slb.msg.api;

import java.io.Serializable;

public class ListenerPortAndProtocol implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String listenerId;
	private Integer listenerPort;
	private String listenerProtocol;
	
	public String getListenerId() {
		return listenerId;
	}
	public void setListenerId(String listenerId) {
		this.listenerId = listenerId;
	}
	public Integer getListenerPort() {
		return listenerPort;
	}
	public void setListenerPort(Integer listenerPort) {
		this.listenerPort = listenerPort;
	}
	public String getListenerProtocol() {
		return listenerProtocol;
	}
	public void setListenerProtocol(String listenerProtocol) {
		this.listenerProtocol = listenerProtocol;
	}
	
	
}
