package com.gcloud.header.slb.model;

import java.io.Serializable;

import com.gcloud.framework.db.jdbc.annotation.TableField;

public class LoadBalancerModel implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@TableField("id")
	private String loadBalancerId;
	@TableField("name")
	private String loadBalancerName;
	@TableField("status")
	private String loadBalancerStatus;
	private String address;
	private String vpcId;
	@TableField("vswitch_id")
	private String vSwitchId;
	private String createTime;
	private String Listener;
	
	public String getLoadBalancerId() {
		return loadBalancerId;
	}
	public void setLoadBalancerId(String loadBalancerId) {
		this.loadBalancerId = loadBalancerId;
	}
	public String getLoadBalancerName() {
		return loadBalancerName;
	}
	public void setLoadBalancerName(String loadBalancerName) {
		this.loadBalancerName = loadBalancerName;
	}
	public String getLoadBalancerStatus() {
		return loadBalancerStatus;
	}
	public void setLoadBalancerStatus(String loadBalancerStatus) {
		this.loadBalancerStatus = loadBalancerStatus;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getVpcId() {
		return vpcId;
	}
	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}
	public String getvSwitchId() {
		return vSwitchId;
	}
	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getListener() {
		return Listener;
	}
	public void setListener(String listener) {
		Listener = listener;
	}
	
	
}
