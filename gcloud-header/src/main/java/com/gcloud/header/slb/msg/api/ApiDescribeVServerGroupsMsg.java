package com.gcloud.header.slb.msg.api;

import com.gcloud.header.ApiMessage;

public class ApiDescribeVServerGroupsMsg extends ApiMessage {
	private String loadBalancerId;
	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return ApiDescribeVServerGroupsReplyMsg.class;
	}
	public String getLoadBalancerId() {
		return loadBalancerId;
	}
	public void setLoadBalancerId(String loadBalancerId) {
		this.loadBalancerId = loadBalancerId;
	}

}
