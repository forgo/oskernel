package com.gcloud.header.slb.msg.api;

import com.gcloud.header.ApiMessage;

public class ApiDescribeHealthCheckAttributeMsg extends ApiMessage{
	private String resourceId;
	private String protocol;
	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return ApiDescribeHealthCheckAttributeReplyMsg.class;
	}
	public String getResourceId() {
		return resourceId;
	}
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

}
