package com.gcloud.header.identity.user;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;

public class UpdateUserMsg extends ApiMessage {
	private String id;
	private String password;//登录用密码，需外部传输，加密不可逆
	private Boolean gender;//false男true女
	private String email;
	private String mobile;
	private Boolean disable;//true禁用false可用
	private String realName;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Boolean getDisable() {
		return disable;
	}

	public void setDisable(Boolean disable) {
		this.disable = disable;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}

}
