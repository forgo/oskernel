package com.gcloud.header.identity.user;

import com.gcloud.header.ApiMessage;

public class GetUserMsg extends ApiMessage{
	private String id;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public Class replyClazz() {
		return GetUserReplyMsg.class;
	}

}
