package com.gcloud.header.compute.msg.api.vm.senior;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.compute.msg.api.vm.base.ApiStopInstanceReplyMsg;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by yaowj on 2018/9/17.
 */
public class ApiCreateImageMsg extends ApiMessage {

    @NotBlank(message = "::云服务器ID不能为空")
    private String instanceId;
    private String imageName;

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    @Override
    public Class replyClazz() {
        return ApiReplyMessage.class;
    }
}
