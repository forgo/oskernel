package com.gcloud.header.compute.msg.api.vm.base;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class ApiModifyInstanceSpecMsg extends ApiMessage {
	@ApiModel(description = "虚拟机ID", require = true)
	private String instanceId;
	@ApiModel(description = "实例类型ID", require = true)
	private String instanceType;

	@Override
	public Class replyClazz() {
		return ApiModifyInstanceSpecReplyMsg.class;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getInstanceType() {
		return instanceType;
	}

	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}
}
