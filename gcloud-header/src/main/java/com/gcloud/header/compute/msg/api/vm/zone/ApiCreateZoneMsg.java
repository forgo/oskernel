
package com.gcloud.header.compute.msg.api.vm.zone;

import com.gcloud.header.ApiMessage;

public class ApiCreateZoneMsg extends ApiMessage {

    private static final long serialVersionUID = 1L;

    private String zoneName;

    @Override
    public Class replyClazz() {
        return ApiCreateZoneReplyMsg.class;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

}
