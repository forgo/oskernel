package com.gcloud.header.compute.enums;

import java.util.Arrays;

public enum StorageType {
	LOCAL("local", "iscsi", DiskProtocol.ISCSI),
    CEPH("rbd", "rbd", DiskProtocol.RBD),
	CENTRAL("central", "iscsi", DiskProtocol.ISCSI),
    FILE("file", "file", DiskProtocol.FILE);

	private String value;
	private String driver;
	private DiskProtocol protocol;

    StorageType(String value, String driver, DiskProtocol protocol) {
        this.value = value;
        this.driver = driver;
        this.protocol = protocol;
    }

    public static StorageType value(String value){
        return Arrays.stream(StorageType.values()).filter(type -> type.getValue().equals(value)).findFirst().orElse(null);
    }

    public static String getDriver(String storageType){
        StorageType st = StorageType.value(storageType);
        return st == null ? null : st.getDriver();
    }

    public String getValue() {
        return value;
    }

    public String getDriver() {
        return driver;
    }

    public DiskProtocol getProtocol() {
        return protocol;
    }
}
