package com.gcloud.boot.test.workflow.model;

import java.util.ArrayList;
import java.util.List;

public class RepeatResModel {
	private List<RepeatRes> repeatParams = new ArrayList<RepeatRes>();

	public List<RepeatRes> getRepeatParams() {
		return repeatParams;
	}

	public void setRepeatParams(List<RepeatRes> repeatParams) {
		this.repeatParams = repeatParams;
	}
	
}
