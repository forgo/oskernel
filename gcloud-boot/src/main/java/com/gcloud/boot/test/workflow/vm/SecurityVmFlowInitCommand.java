package com.gcloud.boot.test.workflow.vm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.boot.test.workflow.model.Cards;
import com.gcloud.boot.test.workflow.model.SecurityVmCreateParams;
import com.gcloud.boot.test.workflow.model.VmCreateParams;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
@Component
@Scope("prototype")
public class SecurityVmFlowInitCommand extends BaseWorkFlowCommand{

	@Override
	protected Object process() throws Exception {
		SecurityVmCreateParams params = new SecurityVmCreateParams();
		
		List<Cards> masterCards = new ArrayList<Cards>();
		Cards masterCard1 = new Cards();
		masterCard1.setCardName("mng");
		masterCard1.setSubNet("192.168.10.1/24");
		Cards masterCard2 = new Cards();
		masterCard2.setCardName("storage");
		masterCard2.setSubNet("192.168.11.1/24");
		
		masterCards.add(masterCard1);
		masterCards.add(masterCard2);
		
		VmCreateParams masterParams = new VmCreateParams();
		masterParams.setType("master");
		masterParams.setVmNum(4);
		masterParams.setCards(masterCards);
		
		/////////////////////////////
		List<Cards> slaveCards = new ArrayList<Cards>();
		Cards slaveCard1 = new Cards();
		slaveCard1.setCardName("mng");
		slaveCard1.setSubNet("192.168.10.1/24");
		Cards slaveCard2 = new Cards();
		slaveCard2.setCardName("storage");
		slaveCard2.setSubNet("192.168.11.1/24");
		
		slaveCards.add(slaveCard1);
		slaveCards.add(slaveCard2);
		
		VmCreateParams slaveParams = new VmCreateParams();
		slaveParams.setType("slave");
		slaveParams.setVmNum(3);
		slaveParams.setCards(slaveCards);
		
		params.setMaster(masterParams);
		params.setSlave(slaveParams);
		
		return params;
	}

	@Override
	protected Object rollback() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Class<?> getResParamClass() {
		// TODO Auto-generated method stub
		return null;
	}

}
