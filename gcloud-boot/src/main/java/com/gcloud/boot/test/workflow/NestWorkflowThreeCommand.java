package com.gcloud.boot.test.workflow;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.boot.test.workflow.model.NestWorkflowThreeCommandReq;
import com.gcloud.boot.test.workflow.model.NestWorkflowThreeRes;
import com.gcloud.boot.test.workflow.model.TestWorkflowTwoRes;
import com.gcloud.common.util.StringUtils;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
@Component
@Scope("prototype")
public class NestWorkflowThreeCommand  extends BaseWorkFlowCommand {

	@Override
	protected Object process() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("NestWorkflowThreeCommand process....thread id:" + Thread.currentThread().getId());
		NestWorkflowThreeRes res = new NestWorkflowThreeRes();
		res.setNestflowparam("test1");
		if(res.getNestflowparam().equals("test")) {
			throw new GCloudException("process fail");
		}
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("NestWorkflowThreeCommand rollbacking....thread id:" + Thread.currentThread().getId());
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("NestWorkflowThreeCommand timeout....thread id:" + Thread.currentThread().getId());
		
		NestWorkflowThreeRes res = (NestWorkflowThreeRes)getResParams();
		System.out.println("res: food=" + res.getNestflowparam());
		
		if(res.getNestflowparam().equals("test"))
		{
			throw new Exception("nest flow timeout error test");
		}
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		// TODO Auto-generated method stub
		return NestWorkflowThreeCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean judgeExecute() {
		NestWorkflowThreeCommandReq req = (NestWorkflowThreeCommandReq)getReqParams();
		return !StringUtils.isBlank(req.getTestSkip());
	}

}
