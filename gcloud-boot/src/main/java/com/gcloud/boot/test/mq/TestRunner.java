package com.gcloud.boot.test.mq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
//@Component
public class TestRunner implements CommandLineRunner{
	@Autowired
	private HelloSender helloSender;
	@Override
	public void run(String... arg0) throws Exception {
		// TODO Auto-generated method stub
		 helloSender.send();
	}

}
