package com.gcloud.boot.test.workflow;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.boot.test.workflow.model.TestWorkflowThreeReq;
import com.gcloud.boot.test.workflow.model.TestWorkflowThreeRes;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
@Component
@Scope("prototype")
public class TestWorkflowThreeCommand extends BaseWorkFlowCommand{

	@Override
	protected Object process() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("3 process....thread id:" + Thread.currentThread().getId());
		TestWorkflowThreeReq req = (TestWorkflowThreeReq)getReqParams();
		System.out.println("req: habbit=" + req.getHabbit() + ",name=" + req.getName() + ",food=" + req.getFood());
		
		TestWorkflowThreeRes res = new TestWorkflowThreeRes();
		res.setTest("hello");
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("3 rollbacking....thread id:" + Thread.currentThread().getId());
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("3 timeout....thread id:" + Thread.currentThread().getId());
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		// TODO Auto-generated method stub
		return TestWorkflowThreeReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		// TODO Auto-generated method stub
		return null;
	}

}
