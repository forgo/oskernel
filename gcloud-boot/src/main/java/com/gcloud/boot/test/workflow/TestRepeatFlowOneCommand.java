package com.gcloud.boot.test.workflow;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.boot.test.workflow.model.RepeatRes;
import com.gcloud.boot.test.workflow.model.RepeatResModel;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

@Component
@Scope("prototype")
public class TestRepeatFlowOneCommand extends BaseWorkFlowCommand{

	@Override
	protected Object process() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("TestRepeatFlowTwoCommand");
		List<RepeatRes> ress = new ArrayList<RepeatRes>();
		RepeatRes res1 = new RepeatRes();
		res1.setNetCardName("card1");
		res1.setType("mgr");
		
		RepeatRes res2 = new RepeatRes();
		res2.setNetCardName("card2");
		res2.setType("storage");
		
		ress.add(res1);
		ress.add(res2);
		
		RepeatResModel result = new RepeatResModel();
		result.setRepeatParams(ress);
		
		return result;
	}

	@Override
	protected Object rollback() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Class<?> getResParamClass() {
		// TODO Auto-generated method stub
		return null;
	}

}
