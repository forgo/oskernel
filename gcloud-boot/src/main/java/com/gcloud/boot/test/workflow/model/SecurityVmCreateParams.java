package com.gcloud.boot.test.workflow.model;

public class SecurityVmCreateParams {
	private VmCreateParams master;
	private VmCreateParams slave;
	public VmCreateParams getMaster() {
		return master;
	}
	public void setMaster(VmCreateParams master) {
		this.master = master;
	}
	public VmCreateParams getSlave() {
		return slave;
	}
	public void setSlave(VmCreateParams slave) {
		this.slave = slave;
	}
}
