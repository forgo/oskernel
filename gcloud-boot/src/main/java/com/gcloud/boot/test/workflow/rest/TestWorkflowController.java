package com.gcloud.boot.test.workflow.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gcloud.boot.test.workflow.NestWorkflow;
import com.gcloud.boot.test.workflow.TestWorkflow;
import com.gcloud.boot.test.workflow.model.Cards;
import com.gcloud.boot.test.workflow.model.TestParam;
import com.gcloud.boot.test.workflow.vm.SecurityVmFlow;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.core.workflow.mng.IWorkFlowInstanceStepMng;

@RestController
@RequestMapping("/")
public class TestWorkflowController {
	@RequestMapping(value = "/test/workflow")
	public void api(@Validated TestParam param){
		
		/*TestWorkflow flow = new TestWorkflow();
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("name", param.getName());
			params.put("batch", param.getBatch());
			flow.execute(params);
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		NestWorkflow flow = new NestWorkflow();
		try {
			List<Cards> tests = new ArrayList<Cards>();
			Cards card = new Cards();
			card.setCardName("rwrwr");
			card.setSubNet("gfgfd");
			tests.add(card);
			Cards card1 = new Cards();
			card1.setCardName("rwrwr111");
			card1.setSubNet("gfgfd11");
			tests.add(card1);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("batch", param.getBatch());
			params.put("tests", tests);
			flow.execute(params);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/*SecurityVmFlow security = new SecurityVmFlow();
		security.execute(null);*/
		
		/*IWorkFlowInstanceStepMng mng = (IWorkFlowInstanceStepMng)SpringUtil.getBean("workFlowInstanceStepMng");
		System.out.println(mng.getFirstStepRess(202l));*/
	}
}
