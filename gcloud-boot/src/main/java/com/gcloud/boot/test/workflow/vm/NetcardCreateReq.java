package com.gcloud.boot.test.workflow.vm;

import com.gcloud.boot.test.workflow.model.Cards;

public class NetcardCreateReq {
	private Cards repeatParams;
	private String type;
	
	public Cards getRepeatParams() {
		return repeatParams;
	}
	public void setRepeatParams(Cards repeatParams) {
		this.repeatParams = repeatParams;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
