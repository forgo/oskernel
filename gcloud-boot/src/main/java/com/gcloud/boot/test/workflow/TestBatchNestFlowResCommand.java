package com.gcloud.boot.test.workflow;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.boot.test.workflow.model.NestWorkflowThreeCommandReq;
import com.gcloud.boot.test.workflow.model.TestBatchNestFlowResCommandRes;
import com.gcloud.common.util.StringUtils;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

@Component
@Scope("prototype")
public class TestBatchNestFlowResCommand  extends BaseWorkFlowCommand{

	@Override
	protected Object process() throws Exception {
		// TODO Auto-generated method stub
		//汇总返回信息
		TestBatchNestFlowResCommandRes res = new TestBatchNestFlowResCommandRes();
		res.setRollback(true);
		/*if(res.isRollback()) {
			throw new GCloudException("process fail");
		}*/
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
//		throw new GCloudException("rollback fail");
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Class<?> getResParamClass() {
		// TODO Auto-generated method stub
		return TestBatchNestFlowResCommandRes.class;
	}
	
	@Override
	public boolean judgeExecute() {
		return false;
	}

}
