package com.gcloud.boot.test.workflow.model;

public class NestWorkflowThreeRes {
	private String nestflowparam;

	public String getNestflowparam() {
		return nestflowparam;
	}

	public void setNestflowparam(String nestflowparam) {
		this.nestflowparam = nestflowparam;
	}
}
