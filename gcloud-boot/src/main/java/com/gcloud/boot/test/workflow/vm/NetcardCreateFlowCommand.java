package com.gcloud.boot.test.workflow.vm;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.boot.test.workflow.model.NetcardCreateRes;
import com.gcloud.common.util.GenIDUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

import lombok.extern.slf4j.Slf4j;

@Component
@Scope("prototype")
@Slf4j
public class NetcardCreateFlowCommand extends BaseWorkFlowCommand{

	@Override
	protected Object process() throws Exception {
		NetcardCreateReq req = (NetcardCreateReq)getReqParams();
		log.debug("NetcardCreateFlowCommand req.type:" + req.getType());
		
		NetcardCreateRes res = new NetcardCreateRes();
		res.setNetcardId(GenIDUtil.genernateId("net-", "1"));
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		// TODO Auto-generated method stub
		return 180;
	}

	@Override
	protected Class<?> getReqParamClass() {
		// TODO Auto-generated method stub
		return NetcardCreateReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		// TODO Auto-generated method stub
		return NetcardCreateRes.class;
	}

}
