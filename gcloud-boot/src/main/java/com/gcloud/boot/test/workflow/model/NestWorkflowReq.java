package com.gcloud.boot.test.workflow.model;

import java.util.List;

public class NestWorkflowReq {
	private int batch;
	private List<Cards> tests;

	public int getBatch() {
		return batch;
	}

	public void setBatch(int batch) {
		this.batch = batch;
	}

	public List<Cards> getTests() {
		return tests;
	}

	public void setTests(List<Cards> tests) {
		this.tests = tests;
	}
	
}
