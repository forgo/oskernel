package com.gcloud.boot.test.workflow.model;

import java.util.List;

public class TestWorkflowReq {
	private int createNum;
	private List<String> instanceIds;
	
	public int getCreateNum() {
		return createNum;
	}
	public void setCreateNum(int createNum) {
		this.createNum = createNum;
	}
	public List<String> getInstanceIds() {
		return instanceIds;
	}
	public void setInstanceIds(List<String> instanceIds) {
		this.instanceIds = instanceIds;
	}
}
