package com.gcloud.boot.test.workflow.vm;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.boot.test.workflow.model.CreateAttachNetCardFlowReq;
import com.gcloud.core.workflow.core.BaseWorkFlows;

@Component
@Scope("prototype")
public class CreateAttachNetCardTestWorkflow extends BaseWorkFlows{

	@Override
	public String getFlowTypeCode() {
		return "CreateAttachNetCardTestWorkflow";
	}

	@Override
	public void process() {
		
	}

	@Override
	protected Class<?> getReqParamClass() {
		return CreateAttachNetCardFlowReq.class;
	}

	@Override
	public Object preProcess() {
		return null;
	}

}
