package com.gcloud.boot.test.workflow;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.gcloud.boot.test.workflow.model.TestWorkflowReq;
import com.gcloud.core.workflow.core.BaseWorkFlows;
@Component
public class TestWorkflow extends BaseWorkFlows {

	@Override
	public String getFlowTypeCode() {
		return "TestWorkflow";
	}

	@Override
	public void process() {
		//工作流已经执行
		System.out.println("TestWorkflow start...");
	}
	
	@Override
	public String getBatchFiled() {
		return "num";
		//return "instanceIds";
	}

	@Override
	protected Class<?> getReqParamClass() {
		return TestWorkflowReq.class;
	}

	@Override
	public Object preProcess() {
		//工作流执行前执行
		System.out.println("TestWorkflow 准备执行...");
		return null;
	}
}
