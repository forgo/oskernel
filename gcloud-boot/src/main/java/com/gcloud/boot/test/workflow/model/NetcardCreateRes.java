package com.gcloud.boot.test.workflow.model;

public class NetcardCreateRes {
	private String netcardId;

	public String getNetcardId() {
		return netcardId;
	}

	public void setNetcardId(String netcardId) {
		this.netcardId = netcardId;
	}
}
