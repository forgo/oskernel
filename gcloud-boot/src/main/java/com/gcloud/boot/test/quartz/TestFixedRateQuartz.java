package com.gcloud.boot.test.quartz;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.stereotype.Component;

import com.gcloud.core.quartz.GcloudJobBean;
import com.gcloud.core.quartz.annotation.QuartzTimer;

@Component
//告诉Quartz在任务执行成功完毕之后（没有抛出异常），修改JobDetail的JobDataMap备份，以供下一个任务使用
@PersistJobDataAfterExecution
//如果使用了@PersistJobDataAfterExecution注解的话，强烈建议同时使用@DisallowConcurrentExecution注解，以避免当两个同样的job并发执行的时候产生的存储数据迷惑
@DisallowConcurrentExecution
@QuartzTimer(fixedDelay = 5000 * 1000L)
public class TestFixedRateQuartz extends GcloudJobBean {

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub
		System.out.println("TestFixedRateQuartz run ... " + System.currentTimeMillis()/1000);
		try {
			Thread.sleep(2 * 1000l);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}