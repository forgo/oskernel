package com.gcloud.core.workflow.dao;

import com.gcloud.core.workflow.entity.BatchWorkFlow;
import com.gcloud.framework.db.dao.IJdbcBaseDao;

public interface IBatchWorkFlowDao  extends IJdbcBaseDao<BatchWorkFlow, Long>{

}
