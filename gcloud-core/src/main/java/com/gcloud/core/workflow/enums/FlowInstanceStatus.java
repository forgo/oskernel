package com.gcloud.core.workflow.enums;

public enum FlowInstanceStatus {
	WAITING,
	EXECUTING,
	SUCCESS,
	FAILURE,
	ROLLBACKING,
	ROLLBACKED;
}
