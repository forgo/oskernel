package com.gcloud.core.cache.redis.config;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gcloud.core.cache.DistributedLock;
import com.gcloud.core.cache.redis.lock.RedisSpinLock;
import com.gcloud.core.cache.redis.template.JedisClientTemplate;

import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedisPool;

@Configuration
//@ConditionalOnExpression("${spring.cache.type:redis} == true")
public class JedisConfig {
	@Value("${spring.redis.host:}")
    private String host;

    @Value("${spring.redis.port:}")
    private Integer port;

    @Value("${spring.redis.timeout:}")
    private Integer timeout;

    @Value("${spring.redis.pool.max-idle:}")
    private Integer maxIdle;

    @Value("${spring.redis.pool.max-wait:}")
    private Long maxWaitMillis;

    @Value("${spring.redis.pool.max-active:}")
    private Integer maxActive;

    @Value("${spring.redis.password:}")
    private String password;


    @Bean
    public ShardedJedisPool shardedJedisPool() {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        if(maxActive != null){
            jedisPoolConfig.setMaxTotal(maxActive);
        }
        if(maxIdle != null){
            jedisPoolConfig.setMaxIdle(maxIdle);
        }
        if(maxWaitMillis != null){
            jedisPoolConfig.setMaxWaitMillis(maxWaitMillis);
        }
        JedisShardInfo info = new JedisShardInfo(host, port);
        if(StringUtils.isNotBlank(password)){
            info.setPassword(password);
        }
        List<JedisShardInfo> jedisShardInfoList = new ArrayList<JedisShardInfo>();
        jedisShardInfoList.add(info);
        return new ShardedJedisPool(jedisPoolConfig, jedisShardInfoList);
    }

    @Bean
    public JedisClientTemplate jedisClientTemplate(){
        return new JedisClientTemplate();
    }

    @Bean
    public DistributedLock redisLock(){
        return new RedisSpinLock();
    }
}
