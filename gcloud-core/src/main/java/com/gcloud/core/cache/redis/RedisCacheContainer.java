package com.gcloud.core.cache.redis;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.cache.redis.template.JedisClientTemplate;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.core.util.SerializeUtils;

@Component
public class RedisCacheContainer extends CacheContainer {
	
	private static JedisClientTemplate redisClient;
	
	@Override
	public void init() {
		// TODO Auto-generated method stub
		redisClient = (JedisClientTemplate)SpringUtil.getBean("jedisClientTemplate");
	}
	
	@Override
	public Object get(CacheType type, String key) {
		// TODO Auto-generated method stub
		Object result = null;
		try {
			String res = redisClient.hget(getDistributeType(type), key);

			if (res == null) {
				return null;
			}
//			result = res;
			result = SerializeUtils.unSerialize(res);
		} catch (GCloudException e) {
			
			//LOG.error("【RedisCacheContainer】获取缓存失败：" + type + "," + key);
		}
		return result;
	}

	@Override
	public String getString(CacheType type, String key) {
		// TODO Auto-generated method stub
		String result = null;
		result = redisClient.hget(getDistributeType(type), key);
		result = SerializeUtils.unSerialize(result).toString();
		if (result == null) {
			return null;
		}
		return result;
	}

	@Override
	public void put(CacheType type, String key, Object value) {
		// TODO Auto-generated method stub
		redisClient.hset(getDistributeType(type), key,// value.toString());
				SerializeUtils.serialize(value));
	}

	@Override
	public <T> void put(CacheType type, Map<String, T> map) {
		// TODO Auto-generated method stub
		Map<String, String> results = new HashMap<String, String>();
		Iterator<Entry<String, T>> it = map.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, T> entry = it.next();
			results.put(entry.getKey().toString(), //entry.getValue().toString());
					SerializeUtils.serialize(entry.getValue()));
		}
		redisClient.del(getDistributeType(type));
		if(!results.isEmpty())
		{
			redisClient.hmset(getDistributeType(type), results);
		}
	}

	@Override
	public void remove(CacheType type, String key) {
		// TODO Auto-generated method stub
		redisClient.hdel(getDistributeType(type), key);
	}
	
	public String getDistributeType(CacheType type) {
		return MessageFormat.format("gcloud_cache_{0}", type.name());
	}

	@Override
	public void put(String key, Object value, int expire) {
		redisClient.set(key, SerializeUtils.serialize(value));
		redisClient.expire(key, expire);
	}

	@Override
	public void refresh(String key, int expire) {
		redisClient.expire(key, expire);
	}

	@Override
	public Object get(String key) {
		Object result = null;
		try {
			String res = redisClient.get(key);

			if (res == null) {
				return null;
			}
			result = SerializeUtils.unSerialize(res);
		} catch (GCloudException e) {
			
		}
		return result;
	}

	@Override
	public void remove(String key) {
		redisClient.del( key);
	}
}
