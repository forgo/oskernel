package com.gcloud.core.cache.redis.lock;

import com.gcloud.core.cache.KeyValueDistributedLock;
import com.gcloud.core.cache.redis.template.JedisClientTemplate;
import com.gcloud.core.exception.GCloudException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class KeyValueRedisLock extends KeyValueDistributedLock {

    @Autowired
    private JedisClientTemplate jedisClient;

    // 指定锁的内容value
    @Override
    public boolean getLock(String lockName, String value, long expireSecondTime) throws GCloudException{
        return jedisClient.setnx(lockName, value, expireSecondTime) == 1L;
    }

    // 返回锁的内容
    @Override
    public String getValueOfLock(String lockName) throws GCloudException{
        return jedisClient.get(lockName);
    }

    public long releaseLock(String lockName) throws GCloudException {
        return jedisClient.del(lockName);
    }
}
