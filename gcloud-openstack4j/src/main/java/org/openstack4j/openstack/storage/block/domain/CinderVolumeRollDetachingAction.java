package org.openstack4j.openstack.storage.block.domain;

import com.fasterxml.jackson.annotation.JsonRootName;
import org.openstack4j.model.ModelEntity;

/**
 * Created by yaowj on 2018/11/7.
 */
@JsonRootName("os-roll_detaching")
public class CinderVolumeRollDetachingAction implements ModelEntity {

    private static final long serialVersionUID = 1L;

}
