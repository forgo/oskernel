package org.openstack4j.openstack.storage.block.domain;

import java.util.List;

import org.openstack4j.model.storage.block.Connector;
import org.openstack4j.model.storage.block.builder.ConnectorBuilder;
import org.openstack4j.openstack.common.ListResult;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("connector")
public class CinderConnector implements Connector {

	private static final long serialVersionUID = 1L;

	private String platform;
	private String host;
	@JsonProperty("do_local_attach")
	private boolean doLocalAttach;
	private String ip;
	@JsonProperty("os_type")
	private String osType;
	private boolean multipath;
	private String initiator;

	@Override
	public ConnectorBuilder toBuilder() {
		return new ConcreteConnectorBuilder(this);
	}

	/**
	 * @return the Volume Type Builder
	 */
	public static ConnectorBuilder builder() {
		return new ConcreteConnectorBuilder();
	}

	public static class Connectors extends ListResult<CinderConnector> {

		private static final long serialVersionUID = 1L;

		@JsonProperty("volume_types")
		private List<CinderConnector> types;

		@Override
		protected List<CinderConnector> value() {
			return types;
		}

	}

	public static class ConcreteConnectorBuilder implements ConnectorBuilder {

		private CinderConnector m;

		ConcreteConnectorBuilder() {
			this(new CinderConnector());
		}

		ConcreteConnectorBuilder(CinderConnector Connector) {
			this.m = Connector;
		}

		@Override
		public Connector build() {
			return m;
		}

		@Override
		public ConnectorBuilder from(Connector in) {
			m = (CinderConnector) in;
			return this;
		}

		@Override
		public ConnectorBuilder platform(String platform) {
			m.platform = platform;
			return this;
		}

		@Override
		public ConnectorBuilder host(String host) {
			m.host = host;
			return this;
		}

		@Override
		public ConnectorBuilder ip(String ip) {
			m.ip = ip;
			return this;
		}

		@Override
		public ConnectorBuilder doLocalAttach(boolean doLocalAttach) {
			m.doLocalAttach = doLocalAttach;
			return this;
		}

		@Override
		public ConnectorBuilder multipath(boolean multipath) {
			m.multipath = multipath;
			return this;
		}

		@Override
		public ConnectorBuilder initiator(String initiator) {
			m.initiator = initiator;
			return this;
		}

		@Override
		public ConnectorBuilder osType(String osType) {
			m.osType = osType;
			return this;
		}
	}

	@Override
	public String getPlatform() {
		return platform;
	}

	@Override
	public String getHost() {
		return host;
	}

	@Override
	public boolean getDoLocalAttach() {
		return doLocalAttach;
	}

	@Override
	public String getIp() {
		return ip;
	}

	@Override
	public String getOsType() {
		return osType;
	}

	@Override
	public boolean getMultipath() {
		return multipath;
	}

	@Override
	public String getInitiator() {
		return initiator;
	}

}
