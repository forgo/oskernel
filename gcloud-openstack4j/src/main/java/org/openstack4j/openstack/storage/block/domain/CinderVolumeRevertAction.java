package org.openstack4j.openstack.storage.block.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import org.openstack4j.model.ModelEntity;

/**
 * Created by yaowj on 2018/11/7.
 */
@JsonRootName("revert")
public class CinderVolumeRevertAction implements ModelEntity {

    private static final long serialVersionUID = 1L;

    @JsonProperty("snapshot_id")
    private String snapshotId;

    public CinderVolumeRevertAction(String snapshotId) {
        this.snapshotId = snapshotId;
    }

    public String getSnapshotId() {
        return snapshotId;
    }

    public void setSnapshotId(String snapshotId) {
        this.snapshotId = snapshotId;
    }
}
