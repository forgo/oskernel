package org.openstack4j.openstack.networking.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.google.common.base.MoreObjects;
import org.openstack4j.model.network.QosBandwidthLimitRule;
import org.openstack4j.model.network.QosBandwidthLimitRuleInfo;
import org.openstack4j.model.network.QosDirection;
import org.openstack4j.model.network.QosMinimumBandwidthRuleInfo;
import org.openstack4j.model.network.QosRuleType;
import org.openstack4j.model.network.builder.QosBandwidthLimitRuleBuilder;

/**
 * Created by yaowj on 2018/10/29.
 * 用于封装get /qos/policies/%s 中的rules
 */
@JsonRootName("bandwidth_limit_rule")
@JsonIgnoreProperties(ignoreUnknown = true)
public class NeutronQosBandwidthLimitRuleInfo implements NeutronQosRuleInfo {

    private static final long serialVersionUID = 1L;

    private String id;
    @JsonProperty("max_kbps")
    private Integer maxKbps;
    @JsonProperty("max_burst_kbps")
    private Integer maxBurstKbps;
    private QosDirection direction;
    private QosRuleType type = QosRuleType.BANDWIDTH_LIMIT;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getMaxKbps() {
        return maxKbps;
    }

    public void setMaxKbps(Integer maxKbps) {
        this.maxKbps = maxKbps;
    }

    public Integer getMaxBurstKbps() {
        return maxBurstKbps;
    }

    public void setMaxBurstKbps(Integer maxBurstKbps) {
        this.maxBurstKbps = maxBurstKbps;
    }

    public QosDirection getDirection() {
        return direction;
    }

    public void setDirection(QosDirection direction) {
        this.direction = direction;
    }

    @Override
    public QosRuleType getType() {
        return type;
    }

    public void setType(QosRuleType type) {
        this.type = type;
    }
}
