package org.openstack4j.openstack.networking.internal;

import org.openstack4j.api.Builders;
import org.openstack4j.api.networking.QosMinimumBandwidthRuleService;
import org.openstack4j.model.network.QosDirection;
import org.openstack4j.model.network.QosMinimumBandwidthRule;
import org.openstack4j.openstack.networking.domain.NeutronQosMinimumBandwidthRule;

/**
 *
 * @author yaowj
 */
public class QosMinimumBandwidthRuleServiceImpl extends BaseNetworkingServices implements QosMinimumBandwidthRuleService {

    @Override
    public QosMinimumBandwidthRule create(String policyId, Integer minKbps, QosDirection direction) {
        QosMinimumBandwidthRule rule = Builders.qosMinimumBandwidthRule().minKbps(minKbps).direction(direction).build();
        return post(NeutronQosMinimumBandwidthRule.class, uri("/qos/policies/%s/minimum_bandwidth_rules", policyId)).entity(rule).execute();
    }
}
