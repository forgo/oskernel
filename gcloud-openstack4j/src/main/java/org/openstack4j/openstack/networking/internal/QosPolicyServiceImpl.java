package org.openstack4j.openstack.networking.internal;

import org.openstack4j.api.networking.QosPolicyService;
import org.openstack4j.model.common.ActionResponse;
import org.openstack4j.model.network.QosPolicy;
import org.openstack4j.openstack.networking.domain.NeutronQosPolicy;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author yaowj
 */
public class QosPolicyServiceImpl extends BaseNetworkingServices implements QosPolicyService {

    @Override
    public QosPolicy create(QosPolicy qosPolicy) {
        checkNotNull(qosPolicy);
        return post(NeutronQosPolicy.class, uri("/qos/policies")).entity(qosPolicy).execute();
    }

    @Override
    public QosPolicy get(String policyId) {
        checkNotNull(policyId);
        return get(NeutronQosPolicy.class, uri("/qos/policies/%s", policyId)).execute();
    }

    @Override
    public ActionResponse delete(String policyId) {
        checkNotNull(policyId);
        return deleteWithResponse(uri("/qos/policies/%s", policyId)).execute();
    }
}
