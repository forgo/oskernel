package org.openstack4j.openstack.networking.internal;

import org.openstack4j.api.Builders;
import org.openstack4j.api.networking.QosBandwidthLimitRuleService;
import org.openstack4j.model.common.ActionResponse;
import org.openstack4j.model.network.QosBandwidthLimitRule;
import org.openstack4j.model.network.QosDirection;
import org.openstack4j.openstack.networking.domain.NeutronQosBandwidthLimitRule;

/**
 *
 * @author yaowj
 */
public class QosBandwidthLimitRuleServiceImpl extends BaseNetworkingServices implements QosBandwidthLimitRuleService {

    @Override
    public QosBandwidthLimitRule create(String policyId, Integer maxKbps, Integer maxBurstKbps, QosDirection direction) {
        QosBandwidthLimitRule rule = Builders.qosBandwidthLimitRule().maxKbps(maxKbps).maxBurstKbps(maxBurstKbps).direction(direction).build();
        return post(NeutronQosBandwidthLimitRule.class, uri("/qos/policies/%s/bandwidth_limit_rules", policyId)).entity(rule).execute();
    }

    @Override
    public QosBandwidthLimitRule update(String policyId, String ruleId, Integer maxKbps, Integer maxBurstKbps, QosDirection direction) {
        QosBandwidthLimitRule rule = Builders.qosBandwidthLimitRule().maxKbps(maxKbps).maxBurstKbps(maxBurstKbps).direction(direction).build();
        return put(NeutronQosBandwidthLimitRule.class, uri("/qos/policies/%s/bandwidth_limit_rules/%s", policyId, ruleId)).entity(rule).execute();
    }

    @Override
    public ActionResponse delete(String policyId, String ruleId) {
        return deleteWithResponse(uri("/qos/policies/%s/bandwidth_limit_rules/%s", policyId, ruleId)).execute();
    }
}
