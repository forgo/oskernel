package org.openstack4j.openstack.networking.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.google.common.base.MoreObjects;
import org.openstack4j.model.network.QosPolicy;
import org.openstack4j.model.network.builder.QosPolicyBuilder;

import java.util.List;

/**
 *
 * @author yaowj
 */
@JsonRootName("policy")
@JsonIgnoreProperties(ignoreUnknown = true)
public class NeutronQosPolicy implements QosPolicy {

    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String description;
    @JsonProperty("project_id")
    private String projectId;
    @JsonProperty("shared")
    private Boolean shared;
    @JsonProperty("is_default")
    private Boolean isDefault;

    @JsonProperty("tenant_id")
    private String tenantId;

    @JsonProperty("rules")
    private List<NeutronQosRuleInfo> rules;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getProjectId() {
        return projectId;
    }

    public Boolean isShared() {
        return shared;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    @Override
    public String getTenantId() {
        return tenantId;
    }

    @Override
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public List<NeutronQosRuleInfo>  getRules() {
        return rules;
    }

    public void setRules(List<NeutronQosRuleInfo> rules) {
        this.rules = rules;
    }

    public NeutronQosPolicy() {

    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).omitNullValues().add("id", id).add("name", name)
                .add("description", description).add("project_id", projectId).add("shared", shared)
                .add("is_default", isDefault).add("tenant_id", tenantId)
                .toString();
    }

    public static QosPolicyBuilder builder() {
        return new QosPolicyConcreteBuilder();
    }

    public static class QosPolicyConcreteBuilder implements QosPolicyBuilder {
        private NeutronQosPolicy m;

        public QosPolicyConcreteBuilder() {
            this(new NeutronQosPolicy());
        }

        public QosPolicyConcreteBuilder(NeutronQosPolicy policy) {
            this.m = policy;
        }

        @Override
        public QosPolicy build() {

            return m;
        }

        @Override
        public QosPolicyBuilder from(QosPolicy in) {
            m = (NeutronQosPolicy) in;
            return this;
        }


        @Override
        public QosPolicyBuilder id(String id) {
            m.id = id;
            return this;
        }

        @Override
        public QosPolicyBuilder name(String name) {
            m.name = name;
            return this;
        }

        @Override
        public QosPolicyBuilder description(String description) {
            m.description = description;
            return this;
        }

        @Override
        public QosPolicyBuilder projectId(String projectId) {
            m.projectId = projectId;
            return this;
        }

        @Override
        public QosPolicyBuilder shared(Boolean isShared) {
            m.shared = isShared;
            return this;
        }

        @Override
        public QosPolicyBuilder isDefault(Boolean isDefault) {
            m.isDefault = isDefault;
            return this;
        }
    }

    @Override
    public QosPolicyBuilder toBuilder() {
        return new QosPolicyConcreteBuilder(this);
    }
}
