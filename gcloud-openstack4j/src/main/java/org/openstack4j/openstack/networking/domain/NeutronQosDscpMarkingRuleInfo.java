package org.openstack4j.openstack.networking.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.google.common.base.MoreObjects;
import org.openstack4j.model.network.QosDscpMarkingRule;
import org.openstack4j.model.network.QosRuleType;
import org.openstack4j.model.network.builder.QosDscpMarkingRuleBuilder;

/**
 * Created by yaowj on 2018/10/29.
 */
@JsonRootName("dscp_marking_rule")
@JsonIgnoreProperties(ignoreUnknown = true)
public class NeutronQosDscpMarkingRuleInfo implements NeutronQosRuleInfo{

    private static final long serialVersionUID = 1L;

    private String id;
    @JsonProperty("dscp_mark")
    private Integer dscpMark;
    private QosRuleType type = QosRuleType.DSCP_MARKING;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getDscpMark() {
        return dscpMark;
    }

    public void setDscpMark(Integer dscpMark) {
        this.dscpMark = dscpMark;
    }

    @Override
    public QosRuleType getType() {
        return type;
    }

    public void setType(QosRuleType type) {
        this.type = type;
    }
}
