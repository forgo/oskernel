package org.openstack4j.openstack.networking.domain;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.databind.annotation.JsonTypeIdResolver;
import org.openstack4j.model.network.QosRuleInfo;

/**
 * Created by yaowj on 2018/10/29.
 */
//@JsonTypeInfo(use = Id.NAME, include = As.EXTERNAL_PROPERTY, property = "type")
//@JsonSubTypes({
//        @Type(value = NeutronQosBandwidthLimitRule.class, name = "BANDWIDTH_LIMIT"),
//        @Type(value = NeutronQosDscpMarkingRule.class, name = "dscp_marking"),
//        @Type(value = NeutronQosMinimumBandwidthRule.class, name = "minimum_bandwidth"),
//
//})

//自定义id解析器，可以用枚举
@JsonTypeInfo(use = Id.CUSTOM, include = As.PROPERTY, property = "type")
@JsonTypeIdResolver(NeutronQosRuleInfoResolver.class)
public interface NeutronQosRuleInfo extends QosRuleInfo {


}
