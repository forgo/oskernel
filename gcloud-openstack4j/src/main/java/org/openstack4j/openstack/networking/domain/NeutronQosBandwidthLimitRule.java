package org.openstack4j.openstack.networking.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.google.common.base.MoreObjects;
import org.openstack4j.model.network.QosBandwidthLimitRule;
import org.openstack4j.model.network.QosDirection;
import org.openstack4j.model.network.QosRuleType;
import org.openstack4j.model.network.builder.QosBandwidthLimitRuleBuilder;

/**
 * Created by yaowj on 2018/10/29.
 */
@JsonRootName("bandwidth_limit_rule")
@JsonIgnoreProperties(ignoreUnknown = true)
public class NeutronQosBandwidthLimitRule implements QosBandwidthLimitRule {

    private String id;
    @JsonProperty("max_kbps")
    private Integer maxKbps;
    @JsonProperty("max_burst_kbps")
    private Integer maxBurstKbps;
    private QosDirection direction;

    public String getId() {
        return id;
    }

    public Integer getMaxKbps() {
        return maxKbps;
    }

    public Integer getMaxBurstKbps() {
        return maxBurstKbps;
    }

    public QosDirection getDirection() {
        return direction;
    }

    public NeutronQosBandwidthLimitRule() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).omitNullValues().add("id", id).add("max_kbps", maxKbps)
                .add("max_burst_kbps", maxBurstKbps).add("direction", direction)
                .toString();
    }

    @Override
    public QosBandwidthLimitRuleConcreteBuilder toBuilder() {
        return new QosBandwidthLimitRuleConcreteBuilder(this);
    }

    public static QosBandwidthLimitRuleConcreteBuilder builder() {
        return new QosBandwidthLimitRuleConcreteBuilder();
    }

    public static class QosBandwidthLimitRuleConcreteBuilder implements QosBandwidthLimitRuleBuilder {
        private NeutronQosBandwidthLimitRule m;

        public QosBandwidthLimitRuleConcreteBuilder() {
            this(new NeutronQosBandwidthLimitRule());
        }

        public QosBandwidthLimitRuleConcreteBuilder(NeutronQosBandwidthLimitRule rule) {
            this.m = rule;
        }

        @Override
        public QosBandwidthLimitRule build() {
            return m;
        }

        @Override
        public QosBandwidthLimitRuleConcreteBuilder from(QosBandwidthLimitRule in) {
            m = (NeutronQosBandwidthLimitRule) in;
            return this;
        }

        @Override
        public QosBandwidthLimitRuleBuilder id(String id) {
            m.id = id;
            return this;
        }

        @Override
        public QosBandwidthLimitRuleBuilder maxKbps(Integer maxKbps) {
            m.maxKbps = maxKbps;
            return this;
        }

        @Override
        public QosBandwidthLimitRuleBuilder maxBurstKbps(Integer maxBurstKbps) {
            m.maxBurstKbps = maxBurstKbps;
            return this;
        }

        @Override
        public QosBandwidthLimitRuleBuilder direction(QosDirection direction) {
            m.direction = direction;
            return this;
        }
    }
}
