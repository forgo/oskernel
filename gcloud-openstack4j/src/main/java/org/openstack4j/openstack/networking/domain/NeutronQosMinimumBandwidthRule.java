package org.openstack4j.openstack.networking.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.google.common.base.MoreObjects;
import org.openstack4j.model.network.QosDirection;
import org.openstack4j.model.network.QosMinimumBandwidthRule;
import org.openstack4j.model.network.builder.QosMinimumBandwidthRuleBuilder;

/**
 * Created by yaowj on 2018/10/29.
 */
@JsonRootName("minimum_bandwidth_rule")
@JsonIgnoreProperties(ignoreUnknown = true)
public class NeutronQosMinimumBandwidthRule implements QosMinimumBandwidthRule {

    private String id;
    @JsonProperty("min_kbps")
    private Integer minKbps;
    private QosDirection direction;

    public String getId() {
        return id;
    }

    public Integer getMinKbps() {
        return minKbps;
    }

    public QosDirection getDirection() {
        return direction;
    }


    public NeutronQosMinimumBandwidthRule() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).omitNullValues().add("id", id).add("min_kbps", minKbps)
                .add("direction", direction)
                .toString();
    }

    @Override
    public QosMinimumBandwidthRuleConcreteBuilder toBuilder() {
        return new QosMinimumBandwidthRuleConcreteBuilder(this);
    }

    public static QosMinimumBandwidthRuleConcreteBuilder builder() {
        return new QosMinimumBandwidthRuleConcreteBuilder();
    }

    public static class QosMinimumBandwidthRuleConcreteBuilder implements QosMinimumBandwidthRuleBuilder {
        private NeutronQosMinimumBandwidthRule m;

        public QosMinimumBandwidthRuleConcreteBuilder() {
            this(new NeutronQosMinimumBandwidthRule());
        }

        public QosMinimumBandwidthRuleConcreteBuilder(NeutronQosMinimumBandwidthRule rule) {
            this.m = rule;
        }

        @Override
        public QosMinimumBandwidthRule build() {
            return m;
        }

        @Override
        public QosMinimumBandwidthRuleConcreteBuilder from(QosMinimumBandwidthRule in) {
            m = (NeutronQosMinimumBandwidthRule) in;
            return this;
        }

        @Override
        public QosMinimumBandwidthRuleBuilder id(String id) {
            m.id = id;
            return this;
        }

        @Override
        public QosMinimumBandwidthRuleBuilder minKbps(Integer minKbps) {
            m.minKbps = minKbps;
            return this;
        }

        @Override
        public QosMinimumBandwidthRuleBuilder direction(QosDirection direction) {
            m.direction = direction;
            return this;
        }
    }
}
