package org.openstack4j.openstack.networking.internal;

import org.openstack4j.api.Builders;
import org.openstack4j.api.networking.QosDscpMarkingRuleService;
import org.openstack4j.model.network.QosDscpMarkingRule;
import org.openstack4j.openstack.networking.domain.NeutronQosDscpMarkingRule;

/**
 *
 * @author yaowj
 */
public class QosDscpMarkingRuleServiceImpl extends BaseNetworkingServices implements QosDscpMarkingRuleService {

    @Override
    public QosDscpMarkingRule create(String policyId, Integer dscpMark) {
        QosDscpMarkingRule rule = Builders.qosDscpMarkingRule().dscpMarking(dscpMark).build();
        return post(NeutronQosDscpMarkingRule.class, uri("/qos/policies/%s/dscp_marking_rules", policyId)).entity(rule).execute();
    }
}
