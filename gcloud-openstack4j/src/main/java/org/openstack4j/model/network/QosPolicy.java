package org.openstack4j.model.network;

import org.openstack4j.common.Buildable;
import org.openstack4j.model.common.Resource;
import org.openstack4j.model.network.builder.QosPolicyBuilder;
import org.openstack4j.openstack.networking.domain.NeutronQosRuleInfo;

import java.util.List;

/**
 * Qos policy for Neutron Port
 *
 * @author yaowj
 */

public interface QosPolicy extends Resource, Buildable<QosPolicyBuilder> {

    String getDescription();

    String getProjectId();

    Boolean isShared();

    Boolean getIsDefault();

    List<NeutronQosRuleInfo>  getRules();


}
