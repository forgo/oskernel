package org.openstack4j.model.network.builder;

import org.openstack4j.common.Buildable.Builder;
import org.openstack4j.model.network.QosDirection;
import org.openstack4j.model.network.QosMinimumBandwidthRule;

/**
 * Created by yaowj on 2018/10/26.
 */
public interface QosMinimumBandwidthRuleBuilder extends Builder<QosMinimumBandwidthRuleBuilder, QosMinimumBandwidthRule> {

    QosMinimumBandwidthRuleBuilder id(String id);
    QosMinimumBandwidthRuleBuilder minKbps(Integer minKbps);
    QosMinimumBandwidthRuleBuilder direction(QosDirection direction);

}
