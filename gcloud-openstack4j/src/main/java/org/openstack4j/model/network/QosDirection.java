package org.openstack4j.model.network;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;


/**
 * The type of Network
 * 
 * @author Jeremy Unruh
 */
public enum QosDirection {
	EGRESS,
  	INGRESS;

	@JsonCreator
	public static QosDirection forValue(String value) {
		if (value != null)
		{
			for (QosDirection s : QosDirection.values()) {
				if (s.name().equalsIgnoreCase(value))
					return s;
			}
		}
		return null;
	}
	
	@JsonValue
	public String toJson() {
		return name().toLowerCase();
	}
	
}
