package org.openstack4j.model.network.builder;

import org.openstack4j.common.Buildable.Builder;
import org.openstack4j.model.network.QosBandwidthLimitRule;
import org.openstack4j.model.network.QosDirection;

/**
 * Created by yaowj on 2018/10/26.
 */
public interface QosBandwidthLimitRuleBuilder extends Builder<QosBandwidthLimitRuleBuilder, QosBandwidthLimitRule> {

    QosBandwidthLimitRuleBuilder id(String id);
    QosBandwidthLimitRuleBuilder maxKbps(Integer maxKbps);
    QosBandwidthLimitRuleBuilder maxBurstKbps(Integer maxBurstKbps);
    QosBandwidthLimitRuleBuilder direction(QosDirection direction);

}
