package org.openstack4j.model.network;

import org.openstack4j.common.Buildable;
import org.openstack4j.model.network.builder.QosDscpMarkingRuleBuilder;

/**
 * Created by yaowj on 2018/10/26.
 */
public interface QosDscpMarkingRule extends QosRule, Buildable<QosDscpMarkingRuleBuilder> {

    Integer getDscpMark();

}
