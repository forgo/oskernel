package org.openstack4j.model.network;

import org.openstack4j.common.Buildable;
import org.openstack4j.model.network.builder.QosBandwidthLimitRuleBuilder;

/**
 * Created by yaowj on 2018/10/26.
 */
public interface QosBandwidthLimitRule extends QosRule, Buildable<QosBandwidthLimitRuleBuilder> {

    Integer getMaxKbps();
    Integer getMaxBurstKbps();
    QosDirection getDirection();

}
