package org.openstack4j.api.networking;

import org.openstack4j.common.RestService;
import org.openstack4j.model.common.ActionResponse;
import org.openstack4j.model.network.QosPolicy;

/**
 *
 * @author yaowj
 */
public interface QosPolicyService extends RestService {

	QosPolicy create(QosPolicy qosPolicy);

	QosPolicy get(String policyId);

	ActionResponse delete(String policyId);

}
