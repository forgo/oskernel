package org.openstack4j.api.networking;

import org.openstack4j.common.RestService;
import org.openstack4j.model.network.QosDirection;
import org.openstack4j.model.network.QosMinimumBandwidthRule;

/**
 *
 * @author yaowj
 */
public interface QosMinimumBandwidthRuleService extends RestService {

	QosMinimumBandwidthRule create(String policyId, Integer minKbps, QosDirection direction);

}
