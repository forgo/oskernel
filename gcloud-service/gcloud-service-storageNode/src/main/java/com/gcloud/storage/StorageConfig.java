
package com.gcloud.storage;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StorageConfig {

    @Value("${gcloud.service.storageNode}")
    public String syncQueue;

    @Value("${gcloud.service.storageNode}" + "_async")
    public String asyncQueue;

    @Bean
    public Queue syncQueue() {
        System.out.println("init storage sync queue:" + syncQueue);
        return new Queue(syncQueue);
    }

    @Bean
    public Queue asyncQueue() {
        System.out.println("init storage async queue:" + asyncQueue);
        return new Queue(asyncQueue);
    }

}
