package com.gcloud.compute.virtual.libvirt.volume;

import com.gcloud.common.util.StringUtils;
import com.gcloud.common.util.SystemUtil;
import com.gcloud.compute.prop.ComputeNodeProp;
import com.gcloud.compute.util.ConfigFileUtil;
import com.gcloud.header.compute.enums.Unit;
import com.gcloud.header.storage.model.VmVolumeDetail;
import com.gcloud.service.common.compute.uitls.DiskQemuImgUtil;
import org.dom4j.Document;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component("VmVolumeRbdImpl")
public class VmVolumeRbdImpl implements IVmVolume {

	@Autowired
	private ComputeNodeProp computeNodeProp;

	@Override
	public void addDiskDevice(Document doc, VmVolumeDetail vmDisk, boolean isCreateElement) {
		Element disk = null;
		if (!isCreateElement) {
			Element devices = (Element) doc.selectSingleNode("/domain/devices");
			disk = devices.addElement("disk");
		} else {
			disk = doc.addElement("disk");
		}

		disk.addAttribute("type", "network");
		disk.addAttribute("device", "disk");

		Element driver = disk.addElement("driver");
		driver.addAttribute("name", "qemu");
		driver.addAttribute("type", "raw");

		// 处理磁盘缓存
		ConfigFileUtil.handleDiskCacheType(driver);

		Element auth = disk.addElement("auth");
		auth.addAttribute("username", computeNodeProp.getRbdUserName());
		Element secret = auth.addElement("secret");
		secret.addAttribute("type", "ceph");
		secret.addAttribute("uuid", computeNodeProp.getRbdSecretUuid());

		Element source = disk.addElement("source");
		source.addAttribute("protocol", "rbd");
		source.addAttribute("name", vmDisk.getSourcePath());

		String res = SystemUtil.run(new String[] { "ceph", "mon", "stat" });
		Pattern pattern = Pattern.compile("(?<=\\{)(.+?)(?=\\})");
		Matcher matcher = pattern.matcher(res);
		String monListStr = "";
		while (matcher.find()) {
			monListStr = matcher.group();
		}

		if (StringUtils.isNotBlank(monListStr)) {
			String[] monList = monListStr.split(",");
			if (monList != null && monList.length > 0) {
				for (String mon : monList) {
					String[] address = mon.split(":");
					Element host = source.addElement("host");
					if (address != null && address.length == 2) {
						host.addAttribute("name", address[0].split("=")[1]);
						host.addAttribute("port", address[1].split("/")[0]);
					}
				}
			}
		}

		Element target = disk.addElement("target");
		target.addAttribute("dev", vmDisk.getTargetDev());
		target.addAttribute("bus", vmDisk.getBusType());

		Element serial = disk.addElement("serial");
		serial.setText(vmDisk.getVolumeRefId());

//		Element address = disk.addElement("address");
//		address.addAttribute("type", "pci");
//		address.addAttribute("domain", "0x0000");
//		address.addAttribute("bus", "0x00");
//		address.addAttribute("slot", "0x05");
//		address.addAttribute("function", "0x0");

		// List<Element> disks = (List<Element>)
		// doc.selectNodes("/domain/devices/disk");
		// if (disks.size() > 0) {
		// Element disk = disks.get(0);
		// Element diskClone = (Element) disk.clone();
		//
		// Element devices = (Element) doc.selectSingleNode("/domain/devices");
		// devices.add(diskClone);
		// // 处理磁盘缓存
		// ConfigFileUtil.handleDiskCacheType(diskClone);
		//
		// Element target = (Element) diskClone.selectSingleNode("target");
		// target.attribute("dev").setValue(vmDisk.getTargetDev());
		//
		// Element address = (Element) diskClone.selectSingleNode("address");
		// address.attribute("slot").setValue("0x05");
		//
		// Element source = (Element) diskClone.selectSingleNode("source");
		// source.addAttribute("name", vmDisk.getSourcePath());
		//
		// if (vmDisk.getConnections() != null && vmDisk.getConnections().size()
		// > 0) {
		// for (DiskConnection diskConnection : vmDisk.getConnections()) {
		// Element host = (Element) source.selectSingleNode("host");
		// host.addAttribute("name", diskConnection.getHost());
		// host.addAttribute("port", diskConnection.getPort());
		// }
		// }
		//
		// Element serial = (Element) diskClone.selectSingleNode("serial");
		// serial.setText(vmDisk.getVolumeId());
		// }
	}

	@Override
	public void createDiskFile(String imageId, String volumeId, Integer systemSize) {
		// TODO 存储池也要传过来
		String systemPath = String.format("rbd:images/%s", imageId);
		if (StringUtils.isBlank(imageId)) {
			DiskQemuImgUtil.create(systemPath, systemSize, Unit.G);
		} else {
			String imagePath = String.format("rbd:volumes/volume-%s", imageId);
			DiskQemuImgUtil.create(systemPath, imagePath, systemSize, Unit.G);
		}
	}

}
