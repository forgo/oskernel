package com.gcloud.compute.virtual.libvirt.volume;

import com.gcloud.compute.prop.ComputeNodeProp;
import com.gcloud.compute.util.ConfigFileUtil;
import com.gcloud.header.storage.model.VmVolumeDetail;
import org.dom4j.Document;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("VmVolumeFileImpl")
public class VmVolumeFileImpl implements IVmVolume {

	@Autowired
	private ComputeNodeProp computeNodeProp;

	@Override
	public void addDiskDevice(Document doc, VmVolumeDetail vmDisk, boolean isCreateElement) {
		Element diskE = null;
		if (!isCreateElement) {
			Element devices = (Element) doc.selectSingleNode("/domain/devices");
			diskE = devices.addElement("disk");
		} else {
			diskE = doc.addElement("disk");
		}

		diskE.addAttribute("device", "disk");
		diskE.addAttribute("type", "file");

		Element driverE = diskE.addElement("driver");
		driverE.addAttribute("name", "qemu");
		driverE.addAttribute("type", vmDisk.getFileFormat());

		// 处理磁盘缓存
		ConfigFileUtil.handleDiskCacheType(driverE);

		Element sourceE = diskE.addElement("source");
		sourceE.addAttribute("file", vmDisk.getSourcePath());

		Element targetE = diskE.addElement("target");
		targetE.addAttribute("dev", vmDisk.getTargetDev());
		targetE.addAttribute("bus", "virtio");

	}

	@Override
	public void createDiskFile(String imageId, String volumeId, Integer systemSize) {

	}

}
