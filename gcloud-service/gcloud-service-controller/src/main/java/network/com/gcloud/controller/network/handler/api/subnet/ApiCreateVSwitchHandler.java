package com.gcloud.controller.network.handler.api.subnet;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.model.CreateVSwitchParams;
import com.gcloud.controller.network.service.ISubnetService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.CreateVSwitchMsg;
import com.gcloud.header.network.msg.api.CreateVSwitchReplyMsg;
@GcLog(taskExpect="创建交换机成功")
@ApiHandler(module=Module.ECS,subModule=SubModule.VSWITCH,action="CreateVSwitch")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.NETWORK, resourceIdField = "vpcId")
public class ApiCreateVSwitchHandler extends MessageHandler<CreateVSwitchMsg, CreateVSwitchReplyMsg> {
	
	@Autowired
	ISubnetService subnetService;
	
	@Override
	public CreateVSwitchReplyMsg handle(CreateVSwitchMsg msg) throws GCloudException {
		CreateVSwitchParams params = BeanUtil.copyProperties(msg, CreateVSwitchParams.class);
		CreateVSwitchReplyMsg reply = new CreateVSwitchReplyMsg();
		reply.setvSwitchId(subnetService.createSubnet(params, msg.getCurrentUser()));
		
		msg.setObjectId(reply.getvSwitchId());
		msg.setObjectName(msg.getvSwitchName());
		return reply;
	}

}
