package com.gcloud.controller.network.handler.api.subnet;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.service.ISubnetService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.DeleteVSwitchMsg;
@GcLog(taskExpect="删除交换机成功")
@ApiHandler(module=Module.ECS,subModule=SubModule.VSWITCH,action="DeleteVSwitch")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.SUBNET, resourceIdField = "vSwitchId")
public class ApiDeleteVSwitchHandler extends MessageHandler<DeleteVSwitchMsg, ApiReplyMessage> {
	@Autowired
	ISubnetService subnetService;
	
	@Override
	public ApiReplyMessage handle(DeleteVSwitchMsg msg) throws GCloudException {
		subnetService.deleteSubnet(msg.getvSwitchId());
		
		msg.setObjectId(msg.getvSwitchId());
		return new ApiReplyMessage();
	}

}
