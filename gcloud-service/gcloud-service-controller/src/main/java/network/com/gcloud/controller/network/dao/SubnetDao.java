package com.gcloud.controller.network.dao;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.network.entity.Subnet;
import com.gcloud.controller.network.model.DescribeVSwitchesParams;
import com.gcloud.core.currentUser.policy.enums.UserResourceFilterPolicy;
import com.gcloud.core.currentUser.policy.model.FilterPolicyModel;
import com.gcloud.core.currentUser.policy.service.IUserResourceFilterPolicy;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.header.api.model.CurrentUser;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

/**
 * Created by yaowj on 2018/10/25.
 */
@Repository
public class SubnetDao extends JdbcBaseDaoImpl<Subnet, String> {
	public <E> PageResult<E> describeVSwitches(DescribeVSwitchesParams params, Class<E> clazz, CurrentUser currentUser){
		IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "s.");
		
		StringBuffer sql = new StringBuffer();
		List<Object> listParams = new ArrayList<Object>();

        sql.append("select s.id as vSwitchId,s.network_id as vpcId,s.cidr as cidrBlock,s.name as vSwitchName,s.zone_id as zoneId,s.router_id as vRouterId from gc_subnets s");
        sql.append(" where 1=1");

        if(StringUtils.isNotBlank(params.getVpcId())) {
            sql.append(" and s.network_id = ?");
            listParams.add(params.getVpcId());
        }
        if(StringUtils.isNotBlank(params.getvSwitchId())) {
        	sql.append(" and s.id = ?");
        	listParams.add(params.getvSwitchId());
        }
        
        sql.append(sqlModel.getWhereSql());
        listParams.addAll(sqlModel.getParams());
        
        sql.append(" order by s.create_time desc");

        return findBySql(sql.toString(),listParams, params.getPageNumber(), params.getPageSize(), clazz);
	}
}
