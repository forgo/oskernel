package com.gcloud.controller.network.provider.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceStates;
import com.gcloud.controller.async.CheckNeutronCreateExternalNetworkAsync;
import com.gcloud.controller.network.dao.NetworkDao;
import com.gcloud.controller.network.entity.Network;
import com.gcloud.controller.network.provider.INetworkProvider;
import com.gcloud.controller.provider.NeutronProviderProxy;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.simpleflow.Flow;
import com.gcloud.core.simpleflow.FlowDoneHandler;
import com.gcloud.core.simpleflow.NoRollbackFlow;
import com.gcloud.core.simpleflow.SimpleFlowChain;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.network.msg.api.CreateExternalNetworkMsg;
import com.gcloud.header.network.msg.api.CreateVpcMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class NeutronNetworkProvider implements INetworkProvider {

    @Autowired
    private NeutronProviderProxy proxy;

    @Autowired
    private NetworkDao networkDao;

    @Override
    public ResourceType resourceType() {
        return ResourceType.NETWORK;
    }

    @Override
    public ProviderType providerType() {
        return ProviderType.NEUTRON;
    }

    @Override
    public void createNetwork(String networkId, CreateVpcMsg msg) {
        SimpleFlowChain<String, String> chain = new SimpleFlowChain<>("createNetwork");
        chain.then(new Flow<String>("createNetwork") {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                String id = proxy.createExternalNetwork(msg.getVpcName());
                chain.data(id);
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, String data) {
                proxy.removeExternalNetwork(data);
            }

        }).then(new NoRollbackFlow<String>("save to db") {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                Network network = new Network();
                network.setId(networkId);
                network.setName(msg.getVpcName());
                //network.setStatus(NetworkStatus.AVALIABLE.getEname());
                network.setRegionId(msg.getRegion());
                network.setProvider(providerType().getValue());
                network.setProviderRefId(data);
                network.setTenantId(msg.getCurrentUser().getDefaultTenant());
                networkDao.save(network);

                chain.next();
            }
        }).done(new FlowDoneHandler<String>() {
            @Override
            public void handle(String data) {
                chain.setResult(data);
            }
        }).start();

        if (StringUtils.isNotBlank(chain.getErrorCode())) {
            throw new GCloudException(chain.getErrorCode());
        }
        /*String id = proxy.createExternalNetwork(msg.getVpcName());
        Network network = new Network();
        network.setId(id);
        network.setName(msg.getVpcName());
        //network.setStatus(NetworkStatus.AVALIABLE.getEname());
        networkDao.save(network);*/
    }

    @Override
    public void removeNetwork(String vpcRefId) {
        proxy.removeExternalNetwork(vpcRefId);
    }

    @Override
    public void updateNetwork(String vpcRefId, String vpcName) {
        org.openstack4j.model.network.Network nw = proxy.getExternalNetwork(vpcRefId);
        if (nw != null) {
            nw.setName(vpcName);
            proxy.updateExternalNetwork(vpcRefId, vpcName);
        }
    }

    @Override
    public List<Network> list(Map<String, String> filter) {
        List<org.openstack4j.model.network.Network> networks = proxy.listNetwork(filter);
        List<Network> nwList = new ArrayList<>();
        for (org.openstack4j.model.network.Network n : networks) {
            Network net = new Network();
            net.setName(n.getName());
            net.setStatus(ResourceStates.status(ResourceType.NETWORK, ProviderType.NEUTRON, n.getStatus().name()));
            net.setProvider(providerType().getValue());
            net.setProviderRefId(n.getId());
            net.setUpdatedAt(n.getUpdatedAt());
            // and region...

            nwList.add(net);
        }

        return nwList;
    }

	@Override
	public void createExternalNetwork(String networkId, CreateExternalNetworkMsg msg) {
		// TODO Auto-generated method stub
		SimpleFlowChain<String, String> chain = new SimpleFlowChain<>("createNetwork");
        chain.then(new Flow<String>("createNetwork") {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                String id = proxy.createExternalNetwork(msg.getNetworkName());
                chain.data(id);
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, String data) {
                proxy.removeExternalNetwork(data);
            }

        }).then(new NoRollbackFlow<String>("save to db") {
            @Override
            public void run(SimpleFlowChain chain, String data) {
            	org.openstack4j.model.network.Network networkOpenstatck = proxy.getExternalNetwork(data);
                Network network = new Network();
                network.setId(networkId);
                network.setName(msg.getNetworkName());
                //network.setStatus(EcsNetworkStatus.AVALIABLE.getEname());
                network.setStatus(ResourceStates.status(ResourceType.ROUTER, ProviderType.NEUTRON, networkOpenstatck.getStatus().name()));
                network.setRegionId(msg.getRegion());
                network.setProvider(providerType().getValue());
                network.setProviderRefId(data);
                network.setTenantId(msg.getCurrentUser().getDefaultTenant());
                network.setCreateTime(new Date());
                
                networkDao.save(network);

                chain.next();
            }
        }).done(new FlowDoneHandler<Network>() {
            @Override
            public void handle(Network data) {
            	chain.setResult(data.getId());
                CheckNeutronCreateExternalNetworkAsync checkAsync = new CheckNeutronCreateExternalNetworkAsync();
                checkAsync.setNetworkId(data.getId());
                checkAsync.start();
            }
        }).start();

        if (StringUtils.isNotBlank(chain.getErrorCode())) {
            throw new GCloudException(chain.getErrorCode());
        }
	}
}
