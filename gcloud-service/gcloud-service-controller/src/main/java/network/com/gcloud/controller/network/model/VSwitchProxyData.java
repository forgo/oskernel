package com.gcloud.controller.network.model;

import org.openstack4j.model.network.Network;
import org.openstack4j.model.network.Subnet;

public class VSwitchProxyData {
	
	private Subnet subent;
	private Network network;
	public Subnet getSubent() {
		return subent;
	}
	public void setSubent(Subnet subent) {
		this.subent = subent;
	}
	public Network getNetwork() {
		return network;
	}
	public void setNetwork(Network network) {
		this.network = network;
	}
	
	

}
