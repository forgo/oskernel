package com.gcloud.controller.slb.handler.api.vservergroup;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.slb.service.IVServerGroupService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiRemoveVServerGroupBackendServersMsg;
import com.gcloud.header.slb.msg.api.ApiRemoveVServerGroupBackendServersReplyMsg;
@GcLog(taskExpect = "删除后端服务器")
@ApiHandler(module=Module.SLB,action="RemoveVServerGroupBackendServers")
public class ApiRemoveVServerGroupBackendServersHandler extends MessageHandler<ApiRemoveVServerGroupBackendServersMsg, ApiRemoveVServerGroupBackendServersReplyMsg> {
	@Autowired
	IVServerGroupService vServerGroupService;
	@Override
	public ApiRemoveVServerGroupBackendServersReplyMsg handle(ApiRemoveVServerGroupBackendServersMsg msg)
			throws GCloudException {
		// TODO Auto-generated method stub
		vServerGroupService.removeVServerGroupBackendServers(msg.getvServerGroupId(), msg.getBackendServers());
		return new ApiRemoveVServerGroupBackendServersReplyMsg();
	}

}
