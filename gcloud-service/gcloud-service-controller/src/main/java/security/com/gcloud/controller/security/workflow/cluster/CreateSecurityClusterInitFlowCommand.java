package com.gcloud.controller.security.workflow.cluster;

import com.gcloud.controller.security.model.CreateClusterResponse;
import com.gcloud.controller.security.model.workflow.CreateSecurityClusterInitFlowCommandReq;
import com.gcloud.controller.security.model.workflow.CreateSecurityClusterInitFlowCommandRes;
import com.gcloud.controller.security.service.ISecurityClusterService;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Slf4j
public class CreateSecurityClusterInitFlowCommand extends BaseWorkFlowCommand {

    @Autowired
    private ISecurityClusterService securityClusterService;

    @Override
    protected Object process() throws Exception {

        CreateSecurityClusterInitFlowCommandReq request = (CreateSecurityClusterInitFlowCommandReq)getReqParams();
        CreateSecurityClusterInitFlowCommandRes response = new CreateSecurityClusterInitFlowCommandRes();
        CreateClusterResponse createInfo = request.getCreateClusterInfo();
        response.setBridgeInfos(createInfo.getBridgeInfos());
        response.setClusterId(createInfo.getId());
        response.setComponents(createInfo.getComponents());
        response.setHa(createInfo.getHa());

        return response;
    }

    @Override
    protected Object rollback() throws Exception {
        //TODO
        //清空所有数据库
        CreateSecurityClusterInitFlowCommandReq req = (CreateSecurityClusterInitFlowCommandReq)getReqParams();
        securityClusterService.cleanClusterData(req.getCreateClusterInfo().getId());

        return null;
    }

    @Override
    protected Object timeout() throws Exception {
        return null;
    }

    @Override
    protected Class<?> getReqParamClass() {
        return CreateSecurityClusterInitFlowCommandReq.class;
    }

    @Override
    protected Class<?> getResParamClass() {
        return CreateSecurityClusterInitFlowCommandRes.class;
    }
}
