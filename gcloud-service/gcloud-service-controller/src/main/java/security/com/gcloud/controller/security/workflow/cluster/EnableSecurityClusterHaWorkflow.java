package com.gcloud.controller.security.workflow.cluster;

import com.gcloud.controller.security.model.EnableClusterHaParams;
import com.gcloud.controller.security.model.workflow.EnableSecurityClusterHaWorkflowReq;
import com.gcloud.controller.security.service.ISecurityClusterService;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.core.workflow.core.BaseWorkFlows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Slf4j
public class EnableSecurityClusterHaWorkflow extends BaseWorkFlows {

    @Autowired
    private ISecurityClusterService clusterService;

    @Override
    public String getFlowTypeCode() {
        return "EnableSecurityClusterHaWorkflow";
    }

    @Override
    public Object preProcess() {
        EnableSecurityClusterHaWorkflowReq req = (EnableSecurityClusterHaWorkflowReq)getReqParams();
        EnableClusterHaParams params = BeanUtil.copyBean(req, EnableClusterHaParams.class);

        return clusterService.enableHa(params, req.getCurrentUser());
    }

    @Override
    public void process() {

    }

    @Override
    protected Class<?> getReqParamClass() {
        return EnableSecurityClusterHaWorkflowReq.class;
    }
}
