package com.gcloud.controller.compute.workflow.vm.storage;

import com.gcloud.controller.compute.workflow.model.vm.AttachSystemDiskFlowCommandReq;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.storage.model.VmVolumeDetail;
import com.gcloud.service.common.compute.uitls.DiskUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Slf4j
public class AttachSystemDiskFlowCommand extends BaseWorkFlowCommand {
	@Autowired
	private IVolumeService volumeService;

	@Override
	protected Object process() throws Exception {
		AttachSystemDiskFlowCommandReq req = (AttachSystemDiskFlowCommandReq) getReqParams();
		log.debug(String.format("虚拟机%s 挂载数据盘块设备开始", req.getInstanceId()));
		volumeService.attachVolume(req.getVolumeId(), req.getInstanceId(), DiskUtil.getDeviceMountpoint(req.getTargetDev()), req.getCreateHost(), getTaskId());

		VmVolumeDetail vmInfo = volumeService.volumeDetail(req.getVolumeId(), req.getInstanceId());
		log.debug(String.format("虚拟机%s 挂载数据盘块设备结束", req.getInstanceId()));
		return vmInfo;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return AttachSystemDiskFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return VmVolumeDetail.class;// AttachVolumeFlowCommandRes.class;
	}

}
