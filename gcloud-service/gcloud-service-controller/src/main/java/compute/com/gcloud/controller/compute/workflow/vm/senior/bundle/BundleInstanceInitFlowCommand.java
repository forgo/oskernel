package com.gcloud.controller.compute.workflow.vm.senior.bundle;

import com.gcloud.controller.compute.ControllerComputeProp;
import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.model.node.Node;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.controller.compute.utils.RedisNodesUtil;
import com.gcloud.controller.compute.workflow.model.senior.BundleInstanceInitFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.senior.BundleInstanceInitFlowCommandRes;
import com.gcloud.controller.image.entity.enums.ImagePropertyItem;
import com.gcloud.controller.image.service.IImageService;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.enums.Device;
import com.gcloud.header.storage.model.VmVolumeDetail;
import com.gcloud.service.common.compute.uitls.VmUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by yaowj on 2018/11/30.
 */
@Component
@Scope("prototype")
@Slf4j
public class BundleInstanceInitFlowCommand extends BaseWorkFlowCommand {

    @Autowired
    private InstanceDao instanceDao;

    @Autowired
    private IVmBaseService vmBaseService;

    @Autowired
    private IImageService imageService;

    @Autowired
    private ControllerComputeProp prop;

    @Autowired
    private IVolumeService volumeService;

    @Override
    protected Object process() throws Exception {

        BundleInstanceInitFlowCommandReq req = (BundleInstanceInitFlowCommandReq)getReqParams();

        VmInstance ins = instanceDao.getById(req.getInstanceId());

        BundleInstanceInitFlowCommandRes res = new BundleInstanceInitFlowCommandRes();
        res.setBeginningState(ins.getState());

        Map<String, String> imageProperties = imageService.getImageProperties(ins.getImageId());
        if(imageProperties != null){
            res.setArchitecture(ObjectUtils.toString(imageProperties.get(ImagePropertyItem.ARCHITECTURE.value())));
            res.setOsType(ObjectUtils.toString(imageProperties.get(ImagePropertyItem.OS_TYPE.value())));
        }

        VmVolumeDetail volumeDetail = volumeService.volumeDetail(req.getInstanceId(), Device.VDA);

        Node node = RedisNodesUtil.getComputeNodeByHostName(ins.getHostname());

        String imageFilePath = VmUtil.getBundleTargetPath(VmUtil.getInstanceConfigPath(prop.getInstanceConfigPath(), node.getNodeIp(), req.getInstanceId()));
        res.setImageFilePath(imageFilePath);
        res.setNodeIp(node.getNodeIp());
        res.setVolumeDetail(volumeDetail);

        return res;
    }

    @Override
    protected Object rollback() throws Exception {

        BundleInstanceInitFlowCommandReq req = (BundleInstanceInitFlowCommandReq)getReqParams();
        vmBaseService.cleanState(req.getInstanceId(), req.getInTask());
        return null;
    }

    @Override
    protected Object timeout() throws Exception {
        return null;
    }

    @Override
    protected Class<?> getReqParamClass() {
        return BundleInstanceInitFlowCommandReq.class;
    }

    @Override
    protected Class<?> getResParamClass() {
        return BundleInstanceInitFlowCommandRes.class;
    }
}
