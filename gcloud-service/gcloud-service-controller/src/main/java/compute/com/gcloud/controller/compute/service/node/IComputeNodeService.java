package com.gcloud.controller.compute.service.node;

import com.gcloud.header.compute.msg.node.node.model.ComputeNodeInfo;

public interface IComputeNodeService {

    void computeNodeConnect(ComputeNodeInfo computeNodeInfo, int nodeTimeout);

}
