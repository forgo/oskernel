/*
 * @Date 2016-8-19
 * 
 * @Author zhangzj@g-cloud.com.cn
 * 
 * @Copyright 2015 www.g-cloud.com.cn Inc. All rights reserved.
 * 
 * 
 */
package com.gcloud.controller.compute.cache;

import com.gcloud.core.cache.redis.template.JedisClientTemplate;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.service.common.Consts;

import java.text.MessageFormat;

public class NodeHaCache {

	
	public static void addNodeHa(String hostName, int timeout) throws GCloudException {
		JedisClientTemplate template = SpringUtil.getBean(JedisClientTemplate.class);
		String setName = MessageFormat.format(Consts.RedisKey.GCLOUD_CONTROLLER_COMPUTE_NODE_HA_CACHE, hostName);
		template.set(setName, hostName);
		template.expire(setName, timeout);
	}

	public static void removeNodeHa(String hostName) throws GCloudException {
		JedisClientTemplate template = SpringUtil.getBean(JedisClientTemplate.class);
		String setName = MessageFormat.format(Consts.RedisKey.GCLOUD_CONTROLLER_COMPUTE_NODE_HA_CACHE, hostName);
		template.del(setName);
	}
	
	public static String getNodeHa(String hostName) throws GCloudException {
		JedisClientTemplate template = SpringUtil.getBean(JedisClientTemplate.class);
		String setName = MessageFormat.format(Consts.RedisKey.GCLOUD_CONTROLLER_COMPUTE_NODE_HA_CACHE, hostName);
		return template.get(setName);
	}
}
