package com.gcloud.controller.compute.workflow.vm.base;

import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.dao.InstanceTypeDao;
import com.gcloud.controller.compute.entity.InstanceType;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.workflow.model.vm.ModifyInstanceSpecInitFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.vm.ModifyInstanceSpecInitFlowCommandRes;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by yaowj on 2018/11/20.
 */
@Component
@Scope("prototype")
@Slf4j
public class ModifyInstanceSpecInitFlowCommand extends BaseWorkFlowCommand {

	@Autowired
	private InstanceDao instanceDao;

	@Autowired
	private InstanceTypeDao instanceTypeDao;


	@Override
	protected Object process() throws Exception {
		ModifyInstanceSpecInitFlowCommandReq req = (ModifyInstanceSpecInitFlowCommandReq) getReqParams();
		ModifyInstanceSpecInitFlowCommandRes res = new ModifyInstanceSpecInitFlowCommandRes();
		try {
			VmInstance instance = instanceDao.getById(req.getInstanceId());
			InstanceType instanceType = instanceTypeDao.getById(req.getInstanceType());

			res.setHostName(instance.getHostname());
			res.setCpu(instanceType.getVcpus());
			res.setMemory(instanceType.getMemoryMb());
			res.setTaskId(UUID.randomUUID().toString());
			res.setOrgCpu(instance.getCore());
			res.setOrgMemory(instance.getMemory());
		} catch (Exception ex) {
			errorRollback();
			throw ex;
		}
		return res;
	}

	@Override
	protected Object rollback() throws Exception {

		errorRollback();

		return null;
	}

	private void errorRollback() {
		ModifyInstanceSpecInitFlowCommandReq req = (ModifyInstanceSpecInitFlowCommandReq) getReqParams();
		List<String> updateField = new ArrayList<>();
		VmInstance ins = new VmInstance();
		ins.setId(req.getInstanceId());
		
		ins.setStepState(null);
		updateField.add("stepState");
		ins.setTaskState(null);
		updateField.add("taskState");

		instanceDao.update(ins, updateField);
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return ModifyInstanceSpecInitFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return ModifyInstanceSpecInitFlowCommandRes.class;
	}
}
