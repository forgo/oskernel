package com.gcloud.controller.compute.workflow.model.vm;

import com.gcloud.controller.compute.model.vm.CreateInstanceByImageInitResponse;
import com.gcloud.header.compute.msg.api.model.DiskInfo;

import java.util.List;

public class CreateInstanceFlowInitCommandReq {

    private String systemDiskName;
    private String imageId;
    private String instanceName;
    private Integer systemDiskSize;
    private String systemDiskCategory;
    private Integer cpu;
    private Integer memory;
    private String securityGroupId;

    private String curUserId;

    private String zoneId;
    private List<DiskInfo> dataDisk;

    private CreateInstanceByImageInitResponse preRes;

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public String getSystemDiskName() {
        return systemDiskName;
    }

    public void setSystemDiskName(String systemDiskName) {
        this.systemDiskName = systemDiskName;
    }

    public Integer getSystemDiskSize() {
        return systemDiskSize;
    }

    public void setSystemDiskSize(Integer systemDiskSize) {
        this.systemDiskSize = systemDiskSize;
    }

    public String getSystemDiskCategory() {
        return systemDiskCategory;
    }

    public void setSystemDiskCategory(String systemDiskCategory) {
        this.systemDiskCategory = systemDiskCategory;
    }

    public Integer getCpu() {
        return cpu;
    }

    public void setCpu(Integer cpu) {
        this.cpu = cpu;
    }

    public Integer getMemory() {
        return memory;
    }

    public void setMemory(Integer memory) {
        this.memory = memory;
    }

    public String getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(String securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    public CreateInstanceByImageInitResponse getPreRes() {
        return preRes;
    }

    public void setPreRes(CreateInstanceByImageInitResponse preRes) {
        this.preRes = preRes;
    }

    public String getCurUserId() {
        return curUserId;
    }

    public void setCurUserId(String curUserId) {
        this.curUserId = curUserId;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public List<DiskInfo> getDataDisk() {
        return dataDisk;
    }

    public void setDataDisk(List<DiskInfo> dataDisk) {
        this.dataDisk = dataDisk;
    }
}
