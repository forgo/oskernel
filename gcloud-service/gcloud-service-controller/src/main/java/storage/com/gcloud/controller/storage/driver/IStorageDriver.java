
package com.gcloud.controller.storage.driver;

import com.gcloud.controller.storage.entity.Snapshot;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.model.CreateDiskResponse;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.compute.enums.StorageType;

public interface IStorageDriver {

    StorageType storageType();

    // STORAGE POOL

    void createStoragePool(String poolId, String poolName) throws GCloudException;

    void deleteStoragePool(String poolName) throws GCloudException;

    // VOLUME

    CreateDiskResponse createVolume(Volume volume) throws GCloudException;

    void deleteVolume(Volume volume) throws GCloudException;

    void resizeVolume(Volume volume, int newSize) throws GCloudException;

    void createSnapshot(Volume volume, String snapshotRefId) throws GCloudException;

    void deleteSnapshot(Volume volume, Snapshot snapshot) throws GCloudException;

    void resetSnapshot(Volume volume, Snapshot snapshot) throws GCloudException;

//    List<Volume> listVolume() throws GCloudException;
//    List<Snapshot> listSnapshot() throws GCloudException;
}
