package com.gcloud.controller.storage.service.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.compute.utils.VmControllerUtil;
import com.gcloud.controller.log.util.LongTaskUtil;
import com.gcloud.controller.storage.dao.SnapshotDao;
import com.gcloud.controller.storage.dao.StoragePoolDao;
import com.gcloud.controller.storage.dao.VolumeAttachmentDao;
import com.gcloud.controller.storage.dao.VolumeDao;
import com.gcloud.controller.storage.entity.Snapshot;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.entity.VolumeAttachment;
import com.gcloud.controller.storage.model.CreateDiskParams;
import com.gcloud.controller.storage.model.CreateDiskResponse;
import com.gcloud.controller.storage.model.CreateVolumeParams;
import com.gcloud.controller.storage.model.DescribeDisksParams;
import com.gcloud.controller.storage.provider.IVolumeProvider;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.Device;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.storage.StorageErrorCodes;
import com.gcloud.header.storage.enums.DiskType;
import com.gcloud.header.storage.enums.DiskTypeParam;
import com.gcloud.header.storage.enums.VolumeStatus;
import com.gcloud.header.storage.model.DiskItemType;
import com.gcloud.header.storage.model.VmVolumeDetail;
import com.gcloud.service.common.Consts;
import com.gcloud.service.common.compute.uitls.DiskUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yaowj on 2018/12/4.
 */
@Component
@Transactional(propagation = Propagation.REQUIRED)
public class VolumeServiceImpl implements IVolumeService {

    @Autowired
    private VolumeDao volumeDao;

    @Autowired
    private StoragePoolDao poolDao;

    @Autowired
    private VolumeAttachmentDao volumeAttachmentDao;

    @Autowired
    private SnapshotDao snapshotDao;

    @Autowired
    private StoragePoolDao storagePoolDao;

    @Override
    public String create(CreateDiskParams cdp,  CurrentUser currentUser) {

        CreateVolumeParams params = BeanUtil.copyProperties(cdp, CreateVolumeParams.class);
        params.setDiskType(DiskType.DATA);

        if (StringUtils.isNotBlank(params.getSnapshotId())) {
            Snapshot snapshot = snapshotDao.getById(params.getSnapshotId());
            if (snapshot == null) {
                throw new GCloudException("0060104::找不到对应的快照");
            }
        }

        return createVolume(params, currentUser);
    }

    @Override
    public String createVolume(CreateVolumeParams params,  CurrentUser currentUser) {
        params.setPool(this.poolDao.checkAndGet(params.getZoneId(), params.getDiskCategory()));
        
        String volumeId = StringUtils.genUuid();
        CreateDiskResponse response = this.getProvider(params.getPool().getProvider()).createVolume(volumeId, params, currentUser);

        LongTaskUtil.syncSucc(response.getLogType(), params.getTaskId(), volumeId);

        return volumeId;
    }

    @Override
    public void deleteVolume(String volumeId, String taskId) {
        Volume volume;
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = this.volumeDao.getById(volumeId);
            if (volume == null) {
                return;
            }
            this.checkAvailable(volume);
            this.volumeDao.updateVolumeStatus(volumeId, VolumeStatus.DELETING);
        }
        try {
            this.getProvider(volume.getProvider()).deleteVolume(volume, taskId);
        }
        catch (Exception e) {
            this.handleDeleteVolumeFailed(volumeId);
        }
    }

    @Override
    public PageResult<DiskItemType> describeDisks(DescribeDisksParams params, CurrentUser currentUser) {

        if (params == null) {
            params = new DescribeDisksParams();
        }

        if (StringUtils.isNotBlank(params.getDiskType())) {
            DiskTypeParam diskType = DiskTypeParam.getByValue(params.getDiskType());
            if (diskType == null) {
                throw new GCloudException("0060401::不支持此磁盘类型");
            }
        }
        return volumeDao.describeDisks(params, DiskItemType.class, currentUser);
    }

    @Override
    public void updateVolume(String volumeId, String name) {
        if (StringUtils.isBlank(name)) {
            return;
        }
        Volume volume = volumeDao.checkAndGet(volumeId);

        Volume updateVol = new Volume();
        List<String> updateField = new ArrayList<>();
        updateVol.setId(volumeId);
        updateField.add(updateVol.updateDisplayName(name));
        volumeDao.update(updateVol, updateField);

        this.getProvider(volume.getProvider()).updateVolume(volume.getProviderRefId(), name, null);
    }

    @Override
    public void reserveVolume(String volumeId) {
        Volume volume;
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = volumeDao.checkAndGet(volumeId);
            if (volume.getStatus().equals(VolumeStatus.ATTACHING.value())) {
                return;
            }
            this.checkAvailableOrInUse(volume);
            this.volumeDao.updateVolumeStatus(volumeId, VolumeStatus.ATTACHING.value());
        }
        this.getProvider(volume.getProvider()).reserveVolume(volume.getProviderRefId());
    }

    @Override
    public void unreserveVolume(String volumeId) {
        Volume volume;
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = volumeDao.checkAndGet(volumeId);
            if (!volume.getStatus().equals(VolumeStatus.ATTACHING.value())) {
                return;
            }
            volumeDao.updateVolumeStatus(volumeId, this.volumeAttachmentDao.isAttach(volumeId) ? VolumeStatus.IN_USE.value() : VolumeStatus.AVAILABLE.value());
        }
        this.getProvider(volume.getProvider()).unreserveVolume(volume.getProviderRefId());
    }

    @Override
    public void attachVolume(String volumeId, String instanceId, String mountPoint, String hostname, String taskId) {
        Volume volume;
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = volumeDao.checkAndGet(volumeId);
//            if (!volume.getStatus().equals(VolumeStatus.ATTACHING.getValue())) {
//                throw new GCloudException(StorageErrorCodes.VOLUME_IS_NOT_ATTACHING);
//            }
            if (this.volumeAttachmentDao.findByVolumeIdAndInstanceId(volumeId, instanceId).isEmpty()) {
                String attachmentId = this.getProvider(volume.getProvider()).attachVolume(volume, instanceId, mountPoint, hostname, taskId);
                VolumeAttachment volumeAttachment = new VolumeAttachment();
                volumeAttachment.setId(attachmentId);
                volumeAttachment.setInstanceUuid(instanceId);
                volumeAttachment.setMountpoint(mountPoint);
                volumeAttachment.setVolumeId(volumeId);
                volumeAttachment.setAttachedHost(hostname);
                volumeAttachmentDao.save(volumeAttachment);
            }
            this.volumeDao.updateVolumeStatus(volumeId, VolumeStatus.IN_USE.value());
        }
    }

    @Override
    public void beginDetachingVolume(String volumeId) {
        Volume volume;
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = volumeDao.checkAndGet(volumeId);
            this.checkInUse(volume);
            volumeDao.updateVolumeStatus(volumeId, VolumeStatus.DETACHING.value());
        }
        this.getProvider(volume.getProvider()).beginDetachingVolume(volume);
    }

    @Override
    public void rollDetachingVolume(String volumeId) {
        Volume volume;
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = volumeDao.checkAndGet(volumeId);
            if (!volume.getStatus().equals(VolumeStatus.DETACHING.value())) {
                return;
            }
            this.volumeDao.updateVolumeStatus(volumeId, this.volumeAttachmentDao.isAttach(volumeId) ? VolumeStatus.IN_USE.value() : VolumeStatus.AVAILABLE.value());
        }
        this.getProvider(volume.getProvider()).rollDetachingVolume(volume);
    }

    @Override
    public void detachVolume(String volumeId, String attachmentId) {
        Volume volume;
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = volumeDao.checkAndGet(volumeId);
            if (!volume.getStatus().equals(VolumeStatus.DETACHING.value())) {
                throw new GCloudException(StorageErrorCodes.VOLUME_IS_NOT_DETACHING);
            }
            VolumeAttachment attachment = this.volumeAttachmentDao.getById(attachmentId);
            if (attachment != null) {
                this.getProvider(volume.getProvider()).detachVolume(volume, attachment);
                this.volumeAttachmentDao.deleteById(attachmentId);
            }
            this.volumeDao.updateVolumeStatus(volumeId, this.volumeAttachmentDao.isAttach(volumeId) ? VolumeStatus.IN_USE.value() : VolumeStatus.AVAILABLE.value());
        }
    }

    @Override
    public void resizeVolume(String volumeId, Integer newSize, String taskId) {
        Volume volume;
        synchronized (StringUtils.syncStringObject(volumeId)) {
            volume = this.volumeDao.getById(volumeId);
            if (volume == null) {
                throw new GCloudException(StorageErrorCodes.FAILED_TO_FIND_VOLUME);
            }
            if (volume.getSize() >= newSize) {
                throw new GCloudException(StorageErrorCodes.NEW_SIZE_CANNOT_BE_SMALLER);
            }
            this.checkAvailable(volume);
            if (this.volumeAttachmentDao.isAttach(volumeId)) {
                throw new GCloudException(StorageErrorCodes.VOLUME_IS_ATTACHED);
            }
            this.volumeDao.updateVolumeStatus(volumeId, VolumeStatus.RESIZING);
        }
        try {
            this.getProvider(volume.getProvider()).resizeVolume(volume, newSize, taskId);
        }
        catch (Exception e) {
            this.handleResizeVolumeFailed(volumeId, newSize, taskId);
        }
    }

    @Override
    public Volume getVolume(String volumeId) {
        return volumeDao.findUniqueByProperty("id", volumeId);
    }

    @Override
    public List<VmVolumeDetail> getVolumeInfo(String instanceId) {

        return null;
    }

    @Override
    public VmVolumeDetail volumeDetail(String volumeId, String instanceId) {

        Volume volume = volumeDao.getById(volumeId);
        if (volume == null) {
            return null;
        }

        return volumeDetail(volume, null, instanceId);

    }

    @Override
    public VmVolumeDetail volumeDetail(String instanceId, Device device) {

        Map<String, Object> values = new HashMap<>();
        values.put(VolumeAttachment.INSTANCE_UUID, instanceId);
        values.put(VolumeAttachment.MOUNTPOINT, device.getMountPath());
        VolumeAttachment va = volumeAttachmentDao.findUniqueByProperties(values);
        if (va == null) {
            return null;
        }

        Volume volume = volumeDao.getById(va.getVolumeId());

        return volumeDetail(volume, va, null);
    }

        VmVolumeDetail detail = new VmVolumeDetail();
    private VmVolumeDetail volumeDetail(Volume volume, VolumeAttachment volumeAttachment, String instanceId) {

        if (volumeAttachment == null && StringUtils.isNotBlank(instanceId)) {
            List<VolumeAttachment> volumeAttachments = volumeAttachmentDao.findByVolumeIdAndInstanceId(volume.getId(), instanceId);
            volumeAttachment = volumeAttachments != null && volumeAttachments.size() > 0 ? volumeAttachments.get(0) : null;
        }

        if (volumeAttachment != null) {
            String mountPoint = volumeAttachment.getMountpoint();
            String targetDev = "";
            if (StringUtils.isNotBlank(mountPoint) && mountPoint.lastIndexOf("/") != -1 && !mountPoint.endsWith("/")) {
                targetDev = mountPoint.substring(mountPoint.lastIndexOf("/") + 1);
            }
            detail.setTargetDev(targetDev);

        }else if (StringUtils.isNotBlank(instanceId)) {
            String targetDev = VmControllerUtil.getVolumeMountPoint(instanceId);
            detail.setTargetDev(targetDev);
        }

        StorageType st = StorageType.value(volume.getStorageType());

        detail.setBusType(Consts.DEFAULT_DISK_DRIVER);
        detail.setDiskType(volume.getDiskType());
        detail.setVolumeSize(volume.getSize());
        detail.setVolumeId(volume.getId());
        detail.setVolumeRefId(volume.getProviderRefId());
        detail.setProvider(volume.getProvider());
        detail.setCategory(volume.getCategory());
        detail.setProtocol(st.getProtocol().value());


        StoragePool pool = this.storagePoolDao.checkAndGet(volume.getZoneId(), volume.getCategory());

        detail.setStorageType(st.getValue());
        detail.setSourcePath(DiskUtil.volumeSourcePath(volume.getProviderRefId(), pool.getPoolName(), st));

        return detail;

    }

    private void checkAvailable(Volume volume) throws GCloudException {
        if (!StringUtils.equals(volume.getStatus(), VolumeStatus.AVAILABLE.value())) {
            throw new GCloudException(StorageErrorCodes.VOLUME_IS_NOT_AVAILABLE);
        }
    }

    private void checkAvailableOrInUse(Volume volume) throws GCloudException {
        if (!StringUtils.equals(volume.getStatus(), VolumeStatus.AVAILABLE.value()) && !StringUtils.equals(volume.getStatus(), VolumeStatus.IN_USE.value())) {
            throw new GCloudException(StorageErrorCodes.VOLUME_IS_NOT_AVAILABLE);
        }
    }

    private void checkInUse(Volume volume) throws GCloudException {
        if (!StringUtils.equals(volume.getStatus(), VolumeStatus.IN_USE.value())) {
            throw new GCloudException(StorageErrorCodes.VOLUME_IS_NOT_IN_USE);
        }
    }

    @Override
    public void handleDeleteVolumeSuccess(String volumeId) {
        this.volumeDao.deleteById(volumeId);
    }

    @Override
    public void handleDeleteVolumeFailed(String volumeId) {
        this.volumeDao.updateVolumeStatus(volumeId, VolumeStatus.AVAILABLE);
    }

    @Override
    public void handleAttachVolumeSuccess(String volumeId, String attachmentId, String instanceId, String mountPoint, String hostname) {
        volumeDao.updateVolumeStatus(volumeId, VolumeStatus.IN_USE.value());
        VolumeAttachment volumeAttachment = new VolumeAttachment();
        volumeAttachment.setId(attachmentId);
        volumeAttachment.setInstanceUuid(instanceId);
        volumeAttachment.setMountpoint(mountPoint);
        volumeAttachment.setVolumeId(volumeId);
        volumeAttachment.setAttachedHost(hostname);
        volumeAttachmentDao.save(volumeAttachment);
    }

    @Override
    public void handleResizeVolumeSuccess(String volumeId, int newSize) {
        Volume updateVol = new Volume();
        List<String> updateField = new ArrayList<>();
        updateVol.setId(volumeId);
        updateField.add(updateVol.updateStatus(VolumeStatus.AVAILABLE.value()));
        updateField.add(updateVol.updateSize(newSize));
        this.volumeDao.update(updateVol, updateField);
    }

    @Override
    public void handleResizeVolumeFailed(String volumeId, int newSize, String taskId) {
        this.volumeDao.updateVolumeStatus(volumeId, VolumeStatus.AVAILABLE);
    }

    private IVolumeProvider getProvider(int providerType) {
        IVolumeProvider provider = ResourceProviders.checkAndGet(ResourceType.VOLUME, providerType);
        return provider;
    }

}
