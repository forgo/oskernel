package com.gcloud.controller.storage.model;

import com.gcloud.common.model.PageParams;

/**
 * Created by yaowj on 2018/9/28.
 */
public class DescribeDisksParams extends PageParams {

    private String diskType;
    private String status;
    private String diskName;
    private String instanceId;

    public String getDiskType() {
        return diskType;
    }

    public void setDiskType(String diskType) {
        this.diskType = diskType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDiskName() {
        return diskName;
    }

    public void setDiskName(String diskName) {
        this.diskName = diskName;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }
}
