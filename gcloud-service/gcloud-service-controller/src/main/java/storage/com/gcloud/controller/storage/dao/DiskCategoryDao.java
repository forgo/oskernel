
package com.gcloud.controller.storage.dao;

import com.gcloud.controller.storage.entity.DiskCategory;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import org.springframework.stereotype.Repository;

@Repository
public class DiskCategoryDao extends JdbcBaseDaoImpl<DiskCategory, String> {

}
