
package com.gcloud.controller.storage.provider;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.core.exception.GCloudException;

public interface IStoragePoolProvider extends IResourceProvider {

    String createStoragePool(String poolId, String storageType, String poolName) throws GCloudException;

    void deleteStoragePool(String storageType, String providerRefId, String poolName) throws GCloudException;

}
