package com.gcloud.controller.log.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcloud.controller.log.model.LogFeedbackParams;
import com.gcloud.controller.log.service.ILogService;


@RestController
public class FeedbackController {
	
	@Autowired
	private ILogService logService;
	
	@RequestMapping(value = "/log/feedback")
	public void api(@Validated LogFeedbackParams param){
		logService.feedback(param);
	}
}
