
package com.gcloud.controller.image.provider.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceStates;
import com.gcloud.controller.image.async.UploadGlanceImageAsync;
import com.gcloud.controller.image.dao.ImageDao;
import com.gcloud.controller.image.dao.ImagePropertyDao;
import com.gcloud.controller.image.entity.Image;
import com.gcloud.controller.image.entity.ImageProperty;
import com.gcloud.controller.image.entity.enums.ImagePropertyItem;
import com.gcloud.controller.image.model.CreateImageParams;
import com.gcloud.controller.image.provider.IImageProvider;
import com.gcloud.controller.provider.GlanceProviderProxy;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.simpleflow.Flow;
import com.gcloud.core.simpleflow.FlowDoneHandler;
import com.gcloud.core.simpleflow.NoRollbackFlow;
import com.gcloud.core.simpleflow.SimpleFlowChain;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.ImageOwnerType;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.service.common.compute.model.QemuInfo;
import com.gcloud.service.common.compute.uitls.DiskQemuImgUtil;
import org.openstack4j.api.Builders;
import org.openstack4j.model.image.v2.ContainerFormat;
import org.openstack4j.model.image.v2.DiskFormat;
import org.openstack4j.model.image.v2.builder.ImageBuilder;
import org.openstack4j.openstack.image.v2.domain.PatchOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//import org.openstack4j.model.image.v2.Image;

@Component
public class GlanceImageProvider implements IImageProvider {

    @Autowired
    private GlanceProviderProxy glanceProviderProxy;

    @Autowired
    private ImageDao imageDao;

    @Autowired
    private ImagePropertyDao imagePropertyDao;

    @Override
    public ResourceType resourceType() {
        return ResourceType.IMAGE;
    }

    @Override
    public ProviderType providerType() {
        return ProviderType.GLANCE;
    }

    @Override
    public String createImage(CreateImageParams params, CurrentUser currentUser) throws GCloudException {

        File file = new File(params.getFilePath());
        if (!file.exists()) {
            throw new GCloudException("0090106::镜像文件不存在");
        }

        QemuInfo info = DiskQemuImgUtil.info(params.getFilePath());
        DiskFormat diskFormat = DiskFormat.value(info.getFormat());

        SimpleFlowChain<org.openstack4j.model.image.Image, String> chain = new SimpleFlowChain<>("create image");
        chain.then(new Flow<org.openstack4j.model.image.v2.Image>() {
            @Override
            public void run(SimpleFlowChain chain, org.openstack4j.model.image.v2.Image data) {
                ImageBuilder builder = Builders.imageV2().name(params.getImageName()).containerFormat(ContainerFormat.BARE).diskFormat(diskFormat);

                if (StringUtils.isNotBlank(params.getArchitecture())) {
                    builder.architecture(params.getArchitecture());
                }

                if (StringUtils.isNotBlank(params.getOsType())) {
                    builder.additionalProperty(ImagePropertyItem.OS_TYPE.value(), params.getOsType());
                }

                if (StringUtils.isNotBlank(params.getDescription())) {
                    builder.additionalProperty(ImagePropertyItem.DESCRIPTION.value(), params.getDescription());
                }

                org.openstack4j.model.image.v2.Image image = glanceProviderProxy.createImage(builder.build());
                chain.data(image);
                chain.next();

            }

            @Override
            public void rollback(SimpleFlowChain chain, org.openstack4j.model.image.v2.Image data) {
                glanceProviderProxy.deleteImage(data.getId());
            }
        }).then(new Flow<org.openstack4j.model.image.v2.Image>() {
            @Override
            public void run(SimpleFlowChain chain, org.openstack4j.model.image.v2.Image data) {
                Image image = new Image();
                image.setId(data.getId());
                image.setMinDisk(data.getMinDisk());
                image.setCreatedAt(data.getCreatedAt());
                image.setName(data.getName());
                image.setStatus(ResourceStates.status(ResourceType.IMAGE, ProviderType.GLANCE, data.getStatus().value()));
                image.setOwner(currentUser.getId());
                image.setTenantId(currentUser.getDefaultTenant());
                image.setProvider(providerType().getValue());
                image.setProviderRefId(data.getId());
                //暂时默认都是公共
                image.setOwnerType(ImageOwnerType.SYSTEM.value());
                imageDao.save(image);

                Map<String, String> properties = data.getAdditionalProperties();
                if (properties != null && properties.size() > 0) {

                    for (Map.Entry<String, String> prop : properties.entrySet()) {

                        ImageProperty property = new ImageProperty();
                        property.setImageId(image.getId());
                        property.setName(prop.getKey());
                        property.setValue(prop.getValue());

                        imagePropertyDao.save(property);
                    }
                }

                //架构特殊处理，旧版的openstack AdditionalProperties 没有返回
                if (StringUtils.isBlank(properties.get(ImagePropertyItem.ARCHITECTURE.value())) && StringUtils.isNotBlank(data.getArchitecture())) {
                    ImageProperty property = new ImageProperty();
                    property.setImageId(image.getId());
                    property.setName(ImagePropertyItem.ARCHITECTURE.value());
                    property.setValue(data.getArchitecture());

                    imagePropertyDao.save(property);
                }

                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, org.openstack4j.model.image.v2.Image data) {
                imageDao.deleteById(data.getId());
                imagePropertyDao.deleteByImageId(data.getId());

            }
        }).then(new NoRollbackFlow<org.openstack4j.model.image.v2.Image>() {
            @Override
            public void run(SimpleFlowChain chain, org.openstack4j.model.image.v2.Image data) {
                UploadGlanceImageAsync async = new UploadGlanceImageAsync();
                async.setImage(data);
                async.setFilePath(params.getFilePath());
                async.setTaskId(params.getTaskId());
                async.start();
                chain.next();
            }
        }).done(new FlowDoneHandler<org.openstack4j.model.image.v2.Image>() {
            @Override
            public void handle(org.openstack4j.model.image.v2.Image data) {
                chain.setResult(data.getId());
            }
        }).start();

        if (chain.getErrorCode() != null) {
            throw new GCloudException(chain.getErrorCode());
        }

        return chain.getResult();
    }

    @Override
    public void updateImage(String imageId, String imageName) throws GCloudException {

        List<String> updateField = new ArrayList<>();
        Image image = new Image();
        image.setId(imageId);
        updateField.add(image.updateName(imageName));
        imageDao.update(image, updateField);

        PatchOperation patchOperation = new PatchOperation(PatchOperation.OperationType.REPLACE, "/name", imageName);
        glanceProviderProxy.updateImage(imageId, patchOperation);

    }

    @Override
    public void deleteImage(String imageId) throws GCloudException {

        imageDao.deleteById(imageId);
        imagePropertyDao.deleteByImageId(imageId);

        glanceProviderProxy.deleteImage(imageId);

    }

    public List<Image> listImage(Map<String, String> filters) throws GCloudException {
        List<org.openstack4j.model.image.v2.Image> imageList = glanceProviderProxy.listImage(filters);
        List<Image> list = new ArrayList<>();
        for (org.openstack4j.model.image.v2.Image i : imageList) {
            Image image = new Image();
//            image.setId(i.getId());
            image.setProvider(providerType().getValue());
            image.setProviderRefId(i.getId());
            image.setName(i.getName());
            image.setSize(i.getSize());
            image.setStatus(ResourceStates.status(ResourceType.IMAGE, ProviderType.GLANCE, i.getStatus().value()));
            image.setCreatedAt(i.getCreatedAt());
            image.setUpdatedAt(i.getUpdatedAt());
            image.setMinDisk(i.getMinDisk());
            image.setOwner(i.getOwner());
//            image.setOwnerType(i.getVisibility().value());
            image.setOwnerType("shared");

            list.add(image);
        }

        return list;
    }
}
